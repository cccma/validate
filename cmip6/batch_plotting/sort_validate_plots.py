#!/usr/bin/env python

import tarfile
import  os
import shutil 
import argparse 
import subprocess
from pprint import pprint


realms = ['atmos','land','ocean']


def extract_plots(run_dir):
    """unzips plots.tar.gz

    """

    print 'extracting plots'
    for rootdir, subdir, filelist in os.walk(run_dir):
        for filename in filelist:
            if filename == 'plots.tar.gz':
                input_path = os.path.join(rootdir, filename)
                output = os.path.join(rootdir)
                tar = tarfile.open(input_path)
                tar.extractall(path=output)
                tar.close()


def copy_plots(run_dir,realms):
    """copies all pngs and pdfs to appropriate directories for the run (e.g. all ocean pngs go 
    to all_pngs_ocean, etc)

    """
    print 'copying plots to image directories'
    for realm in realms: 
        for rootdir, subdirs, filelist in os.walk(run_dir):
            for subdir in subdirs:
                if subdir.startswith(realm):
                    new_root =  os.path.join(rootdir, subdir)        
                    for new_rootdir, new_subdir, new_filelist in os.walk(new_root):    
                        for new_filename in new_filelist:        
                            if new_filename.endswith('.pdf') or new_filename.endswith('.png'):
                                source =  os.path.join(new_rootdir, new_filename)
                                dest = run_dir + '/all_plots_' + realm
                                shutil.copy(source, dest)


def make_plot_dirs(run_dir, realms):
    """ makes fresh directories for the plots
    """
    current_dir = os.getcwd()
    os.chdir(run_dir)
    for realm in realms: 
        plot_dir = 'all_plots_' + realm
        if os.path.exists(plot_dir):
            shutil.rmtree(plot_dir)
        os.mkdir(plot_dir)
    os.chdir(current_dir)


def join_pdfs(run_dir,realms):
    """merges all of the pdfs in each pdf directory

    """ 
    current_dir = os.getcwd()
    for realm in realms:
        print 'merging ' + realm  + ' pdfs'
        plot_dir = run_dir + '/all_plots_' + realm
        os.chdir(plot_dir)
        if os.path.isfile('joined.pdf'):
            os.remove('joined.pdf')
        pdfunite_call = 'pdfunite *.pdf '+realm+'_merged.pdf' 
        subprocess.call(pdfunite_call, shell=True)
        os.chdir(current_dir)


def get_args():
    """get arguments through argparse

    """
     
    description = 'unzips plot.tar.gz files made by validate and copies plots to the appropriate directories'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-p', '--run_dir', help='path to the Validate run')
    parser.add_argument('--pdf_merge', default=True, action='store_false', help='if set, merged pdfs will NOT be created')
    args = vars(parser.parse_args())
    return args


def main():
    args = get_args()
    make_plot_dirs(args['run_dir'],realms)
    extract_plots(args['run_dir'])
    copy_plots(args['run_dir'],realms)
    if args['pdf_merge']:
        join_pdfs(args['run_dir'],realms)


######################################## 

if __name__ == "__main__":
    main()
