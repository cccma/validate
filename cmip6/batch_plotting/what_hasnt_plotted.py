#!/usr/bin/env python
import os
from pprint import pprint
import argparse
    
def get_args():
    """Get input arguments
    
    """

    description = """compares the filein the direct data root to the plots in the verification plot directory
                    and lists table/variable pairs for which plots were not produced"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-d', '--direct_data_root', help="direct data root")
    parser.add_argument('-p', '--verification_directory', help="directory containing the set of plots")
    args = vars(parser.parse_args())
    return args


def main():
    args = get_args()

    file_list = []
    for root, subdirs, files in os.walk(args['direct_data_root']):
        for f in files:
            file_info = f.split('_')
            table_var = file_info[1]+ '_' + file_info[0]
            file_list.append(table_var)

    plot_list = []
    for root, subdirs, files in os.walk(args['verification_directory']):
        subdirs[:] = [sd for sd in subdirs if 'all_plots' in sd]
        for filename in files:
            if filename.endswith('.png'):
                f = filename[:-4].split('_')
                table_var = f[2] + '_' + f[0]
                plot_list.append(table_var)


    diff_list = sorted(list(set(plot_list) ^ set(file_list)))
    pprint(diff_list)



if __name__ == "__main__":
    main()
