#!/usr/bin/env python

import sys
import yaml
import os
import argparse
import datetime
import batch_launch_tools as blt
from pprint import pprint
import yaml 

large_chunk_size = 20
small_chunk_size = 2
template_filename = 'conf_template_comprehensive.yaml'

run_params = {
'run'                   : 'CanESM5',
'experiment'            : 'historical',
'cmip6_verification'    : True,
'start_date'            : '1980-01',
'end_date'              : '2005-01',
'direct_data_root'      : '/space/hall2/sitestore/eccc/crd/CMIP6/preliminary/CMIP6/CMIP/CCCma/CanESM5/historical/',
#'direct_data_root'      : '/fs/site2/dev/eccc/crd/ccrn/users/sah002/testdata/CanESM5/historical/',
'observations_root'     : '/space/hall2/sitestore/eccc/crd/ccrn/users/scrd104/obs4comp',
'cmip5_root'            : '/space/hall2/sitestore/eccc/crd/cetus_data/AR5/CMIP5_OTHER_DOWNLOADS/',
'output_root'           : './',
}

def add_frequency(varslist):
    """
    if a frequency is not specified, add the Validate default value of 'mon' to the variable dict
    
    """
    for vardict in varslist:
        if 'frequency' not in vardict.keys():
            vardict['frequency'] = 'mon'


def get_args():
    """ get the input arguments
    
    """
    
    description = 'creates conf.yaml files from a large list of plots and launches Validate in batches'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-f', '--vars_yaml',
                        help="yaml file with describing all plots")
    parser.add_argument('-vp', '--validate_path',
                        help="path to Validate package")
    parser.add_argument('--testing', default=False, action='store_true', 
                        help='if True, validate_batch_launch will not be run')
    args = vars(parser.parse_args())
    return args


def main():  
    args = get_args()
    with open(args['vars_yaml'], 'r') as f:
        varsdict = yaml.load(f)
    blt.make_working_dir('comprehensive', testing=args['testing'])

    for realm in varsdict:
        print realm
        varslist = varsdict[realm]

        add_frequency(varslist)
        
        #separate out variables which are in  mulitple netcdfs files
        final_varslist, slow_varslist = blt.separate_variables_with_multiple_files(varslist, run_params['direct_data_root'])

        #divide the lists into manageable chunks
        divided_final_varslist = blt.divide_list(final_varslist, large_chunk_size)
        divided_slow_varslist = blt.divide_list(slow_varslist, small_chunk_size)

        #launch Validate batches for each chunk 
        blt.run_chunks(divided_final_varslist, realm, args['validate_path'], template_filename,
                        run_params, testing=args['testing'])
        blt.run_chunks(divided_slow_varslist, realm, args['validate_path'], template_filename,  
                        run_params, testing=args['testing']) 
        
   

######################################## 
if __name__ == "__main__":
     main()  
