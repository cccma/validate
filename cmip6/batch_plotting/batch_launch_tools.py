"""
Module containing tools for launching large Validate batches

"""


import os
import shutil
import datetime
import pickle
import subprocess
from tempfile import mkstemp
import time
from jinja2 import Environment, FileSystemLoader
from pprint import pprint


def render_template(validate_path, template_filename, context):
    """ renders the jinja template to give the conf.yaml file
    
    """
    TEMPLATE_ENVIRONMENT = Environment(
        autoescape=False,
        loader=FileSystemLoader(searchpath = validate_path + '/validate/cmip6/batch_plotting/templates/'),
        trim_blocks=False)
    
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)


def make_working_dir(runtype, run_params, testing=False, silent=False): 
    """makes the working directory for this run
        
    """
    
    new_dir = 'validate_' + runtype + '_runs_' + run_params['experiment'] + '_' + run_params['run_info'] + '_' + run_params['pycmor_version'] + '_' +  datetime.datetime.now().strftime("%Y%m%d-%H%M%S")        
    if testing == True:
        new_dir = 'validate_' + runtype         
        try:
            shutil.rmtree(os.path.join(os.getcwd(), new_dir)) 
        except:
            print('no testing directory to remove')
    run_dir = os.mkdir(new_dir)       
    os.chdir(os.path.join(os.getcwd(),new_dir)) 
    if not silent: print(os.getcwd())


def separate_variables_with_multiple_files(varslist, direct_data_root):
    """Pulls out any variables that are spread over mulitple netcdf files and puts them in 
    a separate list so that they can be run in smaller batches. This helps avoid 
    walltime issues for the batches.
    """
    
    multifile_varslist = []
    for rootdir, subdir, files in os.walk(direct_data_root):
        if len(files) > 1:
            filepath = os.path.normpath(rootdir)
            splitpath = filepath.split(os.sep)
            variable = splitpath[-3]
            multifile_varslist.append(variable)    
     
    edited_varslist = [var for var in varslist if var['variable'] not in multifile_varslist]
    slow_varslist = [var for var in varslist if var['variable'] in multifile_varslist]
    return edited_varslist,slow_varslist


def divide_list(varslist, chunk_length):      
    """ divides the list of variables into chunks that can be run in batch 
       mode without exceeding the wall time
    
    """
    separated_list = []
    for i in range(0, len(varslist), chunk_length):  
        short_list = varslist[i:i + chunk_length]    
        separated_list.append(short_list)
    return separated_list


def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with os.fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    os.remove(file_path)
    shutil.move(abs_path, file_path)


def copy_jobscript(chunk_dir_name,validate_path):
    """copy validate_jobscript into the directory for each chunk
    
    """
    src = validate_path + '/validate/resources/validate_jobscript'
    shutil.copy(src,chunk_dir_name)
    jobscript_path = os.path.join(chunk_dir_name, os.path.basename(src)) if os.path.isdir(chunk_dir_name) else chunk_dir_name 

def run_chunks(divided_list, realm, validate_path, template_filename, run_params, testing=False, silent=False):                                                                   
    """takes in a divided list of variables, sets up a directory for each chunk and runs
    validate_batch_launch from that directory
    """
    for i, short_varslist in enumerate(divided_list):
        chunk_dir_name = realm + '_'+ short_varslist[0]['variable'] + '-' + short_varslist[-1]['variable'] +  '_' + str(len(short_varslist))  +  '_' +  datetime.datetime.now().strftime("%M%S%f")
        chunk_dir = os.mkdir(chunk_dir_name)
        context = {
         
            'vars': short_varslist,
            'direct_data_root' : run_params['direct_data_root'], 
            'cmip6_verification' : run_params['cmip6_verification'], 
            'observations_root' : run_params['observations_root'], 
            'cmip5_root' : run_params['cmip5_root'],
            'output_root' : run_params['output_root'],
            'start_date' : run_params['start_date'],
            'end_date' : run_params['end_date'],
            'run' : run_params['run'],
            'experiment' : run_params['experiment'],
            'comp_model' : run_params['comp_model'],
         }  
 
        fname = chunk_dir_name + '/conf.yaml'
        with open(fname, 'w') as f:
            yfile = render_template(validate_path, template_filename, context)
            f.write(yfile)
        copy_jobscript(chunk_dir_name, validate_path)
        os.chdir(os.path.join(os.getcwd(), chunk_dir_name))
        if not silent:
            print(os.getcwd())
            print(datetime.datetime.now())
        if not testing:
            time.sleep(10)
            jobid = subprocess.check_output(validate_path + '/validate/resources/validate_batch_launch')
            print(jobid.strip())
        os.chdir('..')
