#!/usr/bin/env python
import os
import argparse
import datetime
import pickle
from pprint import pprint
import batch_launch_tools as blt
import config as cfg

def add_frequency(varslist):
    """ finds the model frequency for a variable from the CMOR table name and 
    adds it to the dictionary of information on that variable
    
    """

    freqlist = ['fx','yr','mon','day','6hr','3hr','1hr','subhr']
            
    for vardict in varslist:
        varname = vardict['CMOR name']
        table = vardict['CMOR table']
        for freqstring in freqlist:
            if freqstring in table:
                vardict['frequency'] = freqstring
            if table == 'AERhr':
                vardict['frequency'] = '1hr'
            if table == 'Oclim':
                vardict['frequency'] = 'mon'
        if 'frequency' not in vardict.keys():
            print 'frequency could not be determined for '+ varname + ' in ' + table   


def add_realm(varslist, realm):
    """ adds the model realm to the dictionary of information on a variable

    """

    for vardict in varslist:
        vardict['realm'] = realm


def add_variable(varslist):
    """ adds the CMOR variable name to the dictionary of information on a variable

    """

    for vardict in varslist:
        vardict['variable'] = vardict['CMOR name']


def add_comparison_flag(varslist, nocomparison_list):
    """ adds a model comparison flag to the dictionary of information on a variable.
        The default value is True, however if a table/variable combination is in the 
        nocomparison_list (set in config.py), the flag is set to False and a comparison
        with the comp_model is not attempted.
   
    """
    for vardict in varslist:
        vardict['compare_CanESM2'] = True
        if (vardict['CMOR table'], vardict['CMOR name']) in nocomparison_list:
            vardict['compare_CanESM2'] = False 
            print(vardict['CMOR table'], vardict['CMOR name'])


def get_pickled_lists(f):
    """ retrieves the pickled lists made from the verification spreadsheet 

    """

    var_file = open(f,'rb')
    varsdict = pickle.load(var_file)
    var_file.close()

    return varsdict


def deal_with_fixed_height_2d_vars(varsdict):
    """overwrites the 'axes' parameter in the varaible dictionary with an empty 
    list for those variables that have a single, fixed Z dimension so that 
    they are plotted as 2D (in space) rather than 3D variables. 
        
    """
    
    for realm in varsdict:
        varslist = varsdict[realm]
        for vardict in varslist:
            sub_string = 'height'
            single_levels = ['height100m','height10m', 'height2m','depth2000m',
                'depth700m', 'depth300m','depth100m','depth0m','sdepth10','sdepth1',
                'olayer100m','p1700','p850','p840','p700','p560','p500','p220',
                'p200','p1000','p100','p10']
            dims = vardict['dimensions']
            intersection = [value for value in single_levels if value in dims]
            if len(intersection) > 0:
                vardict['axes'] = ['']            


def separate_slow_comparisons(edited_varslist):
    """ separates variables with long run times

    """
    slow_comparison_vars = ['mrsos','mrro','snc','snm','snw','tslsi']
    
    remaining_varslist = [var for var in edited_varslist if var['CMOR name'] not in slow_comparison_vars]
    slow_comparisons_varslist = [var for var in edited_varslist if var['CMOR name'] in slow_comparison_vars]

    return remaining_varslist, slow_comparisons_varslist    



def get_args():
    """ get the input arguments

    """

    description = 'Produces conf.yaml files and launches Validate in batches'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-f', '--pickled_varslist',
                        help="pickled variables list file")
    parser.add_argument('-vp', '--validate_path',
                        help="path to Validate package")
    parser.add_argument('--testing', default=False, action='store_true', 
                        help='if True, validate_batch_launch will not be run')
    parser.add_argument('--silent', default=False, action='store_true',
                        help='if True, only output the resulting batch jobids')
    args = vars(parser.parse_args())
    return args


def main():
    args = get_args()
    varsdict = get_pickled_lists(args['pickled_varslist'])
    deal_with_fixed_height_2d_vars(varsdict)
    blt.make_working_dir('verification', cfg.run_params, testing=args['testing'], silent=args['silent']) 

    for realm in varsdict:
        if not args['silent']: print(realm)

        varslist = varsdict[realm]
        
        #add necessary fields to variable dictionaries
        add_comparison_flag(varslist,cfg.nocomparison_list)
        add_frequency(varslist)
        add_realm(varslist, realm)
        add_variable(varslist)        

        #separate out variables which are in  mulitple netcdfs files
        intermediate_varslist, slow_varslist = blt.separate_variables_with_multiple_files(varslist, cfg.run_params['direct_data_root'])
        
        #spearate out variables for which CanESM2 comparisons are very slow
        final_varslist, slow_comparisons_varslist = separate_slow_comparisons(intermediate_varslist)

        #divide the lists into manageable chunks
        divided_final_varslist = blt.divide_list(final_varslist, cfg.large_chunk_size)
        divided_slow_varslist = blt.divide_list(slow_varslist, cfg.small_chunk_size)
        divided_slow_comparisons_varslist= blt.divide_list(slow_comparisons_varslist, cfg.small_chunk_size)

        #launch Validate batches for each chunk
        blt.run_chunks(divided_final_varslist, realm, args['validate_path'], cfg.template_filename, 
                        cfg.run_params, testing=args['testing'], silent=args['silent']) 
        blt.run_chunks(divided_slow_varslist, realm, args['validate_path'], cfg.template_filename, 
                        cfg.run_params, testing=args['testing'], silent=args['silent']) 
        blt.run_chunks(divided_slow_comparisons_varslist, realm, args['validate_path'], cfg.template_filename, 
                        cfg.run_params, testing=args['testing'], silent=args['silent']) 

        if not args['silent']:
            print('number of large batches: ', len(final_varslist)*1./cfg.large_chunk_size, 'number of small batches: ', 1.*(len(slow_comparisons_varslist) + len(slow_varslist)/cfg.small_chunk_size))

########################################
 
if __name__ == "__main__":
    main()                                                                   

