var load_image = function(image,index,realm) {
        var realm_carousel = '#' + realm + '-all_the_images';     
        var realm_path = realm +'/';
        var variable = image.split('_')[0];
        var table = image.split('_')[2].slice(0,-4);
        var salt = Math.random().toString(36).substring(7);
        var active = index == 0 ? 'active' : '';
        var spreadsheets = {
            atmos: 'gid=1583959998',
            land:  'gid=10690959',
            ocean: 'gid=398164077'
            };
        new_image_tag = '<div class="carousel-item ' + active  + '"> <img class="d-block" src="' + realm_path + image + '?'+salt+'">'+
            '<div class="carousel-link"><a target="_blank" href="'+ realm_path + image + '?'+salt+'">' + image + '</a></div>'+
            '<div class="carousel-link"><a target="_blank" href="'+ realm_path + image.slice(0,-4) + '.pdf?'+salt+'">' + image.slice(0,-4) + '.pdf (hi res)</a></div>'+
            '<div class="carousel-link"><a target="_blank" href=https://docs.google.com/spreadsheets/d/1JIF82CfKhTRhyHXDttRaTkaoMKpA9zRiESoX-zm77SA/edit#"'+ spreadsheets[realm] +'">' + realm + ' spreadsheet</a></div>'+
            '<div class="carousel-link"><a target="_blank" href="/~cmr203/CMIP6/verification/CMIP6/CMIP/CCCma/CanESM5/historical/r1i1p1f1/' + table + '/' + variable + '/gn/v20190306/">netcdf file</a></div>'+
            '</div>';
        $(realm_carousel).append(new_image_tag);
        };
    
var menu_item = function(image,index,realm) {
        new_menu_item = '<option value="'+index+'">' + image.slice(0, -4) + '</option>';             
        $('#'+realm+'-select').append(new_menu_item);
        $('#'+realm+'-select').selectpicker('refresh');
        };
    
var load_carousel = function(realm) {
    $.get(realm + '/', function( content ) {
        var png_list = $(content).find("a:contains('.png')");
        png_list.each(function(index){
            load_image($(this).text(),index,realm);
            menu_item($(this).text(),index,realm);
            });

        $('#'+realm+'-select').change(function(){
            var index = parseInt($(this).val())
            $('#' + realm + '-all_the_images').carousel(index);
            $('#' + realm + '-all_the_images').carousel('pause');
            });
        });
    };
 
$(function() {
    load_carousel("atmos")
    load_carousel("land")
    load_carousel("ocean")
    });
