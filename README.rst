validate
========
Welcome! Validate is a powerful python package that can be used to produce
useful climate simulation visualizations. As of the fall of 2018, the most
recent documentation has been moved to this repos
wiki (https://gitlab.science.gc.ca/CanESM/validate/wikis/home). Users 
are encouraged to visit the various pages there. 

There is also more generic information on the [Read the Docs site](https://validate-climate-model-validation.readthedocs.io/en/latest/)

Contributors
------------
Clint Seinen: clint.seinen@canada.ca

David Fallis:  davidwfallis@gmail.com 

Neil Swart: neil.swart@canada.ca

LICENSE
-------

See the LICENSE.txt file in the validate package. validate is distributed
under the GNU General Public License version 2, and the Open Government 
License - Canada (http://data.gc.ca/eng/open-government-licence-canada)
