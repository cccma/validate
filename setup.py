try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from validate import __version__


setup(
    name = 'validate',
    version = __version__,
    author = 'David W. Fallis, Neil Swart, Clint Seinen',
    author_email = 'davidwfallis@gmail.com; neil.swart@canada.ca; clint.seinen@canada.ca',
    packages = ['validate', 'validate.functions'],
    include_package_data = True,
    scripts = ['bin/validate-configure', 'bin/validate-execute'],
    url = 'https://gitlab.science.gc.ca/ncs001/validate',
    description = 'Climate Model validation package',
    long_description = open('README.rst').read(),
    keywords = ['Climate model', 'validation', 'plots', 'CMIP5', 'CMIP6', 'analysis'],
    install_requires = [
        'brewer2mpl ==1.4.1',
        'cdo ==1.3.0',
        'netCDF4 ==1.5.1.2',
        'matplotlib ==3.1.3',
        'pyyaml==5.1.1',
        'numpy ==1.14.6',
        'cmipdata ==0.7',
        'Cartopy ==0.17.0',
        'cython',
    ],
)

#package_data={'validate':['configure/*.yaml']},
