"""
plot_cases
===============

This module contains functinos for different cases of
plots. The functinos will load the appropriate data. Do
any manipulations needed for the data and direct the data
to the correct plot.

.. moduleauthor:: David Fallis
"""

import os
from . import data_loader as pl
from . import projections as pr
import numpy as np
from numpy import mean, sqrt, square
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.ticker as ticker
from matplotlib import gridspec
from . import defaults as dft
import datetime
from .projections import default_pcolor_args
from .colormaps import viridis, magma, inferno, plasma
from netCDF4 import Dataset, num2date, date2num
import textwrap
import cmocean.cm

colormaps = {'viridis': viridis, 
             'magma':magma, 
             'inferno': inferno, 
             'plasma': plasma}

def _1d_depth_data(data, depth, plot):
    if not type(data) == 'numpy.ndarray':
        plot['plot_depth'] = None
        return data
    plot['plot_depth'] = min(depth, key=lambda x: abs(x - plot['depth']))
    try:
        depth_ind = np.where(np.round(depth) == np.round(plot['plot_depth']))[0][0]
    except:
        print(('Failed to extract depth ' + plot['plot_depth'] + ' for ' + plot['variable']))
        depth_ind = 0
    data = data[depth_ind]
    return data

def _depth_data(data, depth, plot):
    """ Makes a numpy array only containing data at the desired depth

    Parameters
    ----------
    data : numpy array
    depth : numpy array
            the depth needed
    plot : dictionary

    Returns
    -------
    dictionary
    """
    if data.ndim > 2:
        plot['plot_depth'] = min(depth, key=lambda x: abs(x - plot['depth']))
        try:
            depth_ind = np.where(np.round(depth) == np.round(plot['plot_depth']))[0][0]
        except:
            print(('Failed to extract depth ' + plot['plot_depth'] + ' for ' + plot['variable']))
            depth_ind = 0
        data = data[depth_ind, :, :]
    else:
        plot['plot_depth'] = None
    return data

def _full_depth_data(data, depth, plot):
    if data.ndim > 3:
        depth_ind = np.where(np.round(depth) == np.round(plot['plot_depth']))[0][0]
        data = data[:, depth_ind, :, :]
    return data


def _section_data(data, plot):
    """ Averages the data for each latitude

    Parameters
    ----------
    data : numpy array
    plot : dictionary

    Returns
    -------
    numpy array
    """
    if plot['variable'] == 'msftmyz':
        return data[plot['basin'], :, :]
    
    try:
        if data.ndim == 3:
            zonmean = data.mean(axis=2)
        elif data.ndim == 2:
            zonmean = data.mean(axis=1)
    except:
        print('proc_plot cannot zonal mean for section ' + plot['ifile'] + ' ' + plot['variable'])
        return data
    return zonmean


def _pcolor(data, plot, anom=False):
    if anom or plot['divergent']:
        anom = True    
    if not plot['data1']['pcolor_flag']:
        dpa = default_pcolor_args(data, anom, missing_value=plot['missing_value'])
        for key in dpa:
            plot['data1']['pcolor_args'][key] = dpa[key]
    
    _fill_colormap(plot)

def _comp_pcolor(data, obs, plot, anom=False):
    """ Gives the data and observations the same colorbar
        for comparison

    Parameters
    ----------
    data : numpy array
    obs : numpy array
    plot : dictionary
    ptype : string
            'climatology' or 'trends'
    """
    if anom or plot['divergent']:
        anom = True  
    if not plot['data1']['pcolor_flag'] and not plot['data2']['pcolor_flag']:
        d1pca = default_pcolor_args(data, anom, missing_value=plot['missing_value'])
        d2pca = default_pcolor_args(obs, anom, missing_value=plot['missing_value'])

        vmin = np.min([d1pca['vmin'], d2pca['vmin']])
        vmax = np.max([d1pca['vmax'], d2pca['vmax']])

        d1pca['vmin'] = vmin
        d1pca['vmax'] = vmax
        for key in d1pca:
            plot['data1']['pcolor_args'][key] = d1pca[key]
        for key in d1pca:
            plot['data2']['pcolor_args'][key] = d1pca[key]

    _fill_colormap(plot)  

def _fill_colormap(plot):
    try:
        if plot['data1']['colormap'] in colormaps:
            plot['data1']['pcolor_args']['cmap'] = colormaps[plot['data1']['colormap']]
        elif hasattr(cmocean.cm, plot['data1']['colormap']):
            plot['data1']['pcolor_args']['cmap'] = getattr(cmocean.cm, plot['data1']['colormap'])
    except KeyError:
        print("colormap for data1 not found, defaulting to inferno")
    try:
        if plot['data2']['colormap'] in colormaps:
            plot['data2']['pcolor_args']['cmap'] = colormaps[plot['data2']['colormap']]
        elif hasattr(cmocean.cm, plot['data2']['colormap']):
            plot['data2']['pcolor_args']['cmap'] = getattr(cmocean.cm, plot['data2']['colormap'])

    except KeyError:
        print("colormap for data2 not found, defaulting to inferno")
    try:
        if plot['comp']['colormap'] in colormaps:
            plot['comp']['pcolor_args']['cmap'] = colormaps[plot['comp']['colormap']]
        elif hasattr(cmocean.cm, plot['comp']['colormap']):
            plot['comp']['pcolor_args']['cmap'] = getattr(cmocean.cm, plot['comp']['colormap'])

    except KeyError:
        print("colormap for comp not found, defaulting to RdBu")

def savefigures(plotname, png=False, pdf=False, **kwargs):
    pdfname = plotname + '.pdf'
    pngname = plotname + '.png'
    if png:
        plt.savefig(pngname, bbox_inches='tight', dpi=200)
    if pdf:
        plt.savefig(pdfname, bbox_inches='tight')

def plotname(plot):
    if plot['cmip6_verification']:
        plotname = 'plots/'
        plotname += plot['variable']
        plotname += '_' + plot['realm']
        if plot['table_id'] is not None:
            plotname += '_' + plot['table_id']   
    else:
        plotname = 'plots/'
        plotname += plot['variable']
        plotname += '_' + plot['data_type']
        plotname += '_' + plot['plot_projection']
        plotname += '_' + str(plot['plot_depth'])
        plotname += '_' + plot['dates']['start_date'] + plot['dates']['end_date']
        season = ''.join(plot['seasons'])
        plotname += season
        if plot['table_id'] is not None:
            plotname += '_' + plot['table_id']   

        if plot['comp_ensembles']:
            plotname += '_ensemble_comparison'

        try: 
            plotname += '_' + str(plot['basin'])
        except:
            pass
    
        if plot['comp_flag'] == 'Model' or not plot['comp_flag']:
            return plotname
        try:
            plotname += '_' + plot['comp_model']
        except: pass
        plotname += '_' + plot['comp_dates']['start_date'] + plot['comp_dates']['end_date']
        compseason = ''.join(plot['comp_seasons'])
        plotname += compseason

    return plotname    


def weighted_mean(data, weights=None):
    if weights is None:
        weights = np.ones(data.shape)

    flattened_data = data.flatten()
    flattened_weights = weights.flatten()    

    mean = np.ma.average(flattened_data, weights=flattened_weights)    
    return mean
    
        
def stats(plot, data, weights=None, rmse=False):
    if rmse:
        vals = ["{:.2E}".format(np.nanmin(data)), "{:.2E}".format(np.nanmax(data)), 
                "{:.2E}".format(sqrt(weighted_mean(square(data), weights=weights)))]
        snam = ['min: ', 'max: ', 'rmse: ']
        plot['stats'] = {'rmse': float(vals[2]),
                         'min': float(vals[0]),
                         'max': float(vals[1]),
                         }
    else:
        vals = ["{:.2E}".format(np.nanmin(data)), "{:.2E}".format(np.nanmax(data)), 
                "{:.2E}".format(weighted_mean(data, weights=weights))]
        snam = ['min: ', 'max: ', 'mean: ']
        plot['stats'] = {'mean': float(vals[2]),
                         'min': float(vals[0]),
                         'max': float(vals[1]),
                         }
    val = [s + v for s, v in zip(snam, vals)]
    label = '  '.join(val)
    
    return label
    

def colormap(plot):
    """ Loads and plots the data for a time averaged map

    Parameters
    ----------
    plot : dictionary
    func : a method that will plot the data on a specified map

    Returns
    -------
    string : name of the plot
    """
    print('plotting map of ' + plot['variable'])
    # load data from netcdf file
    data, lon, lat, depth, units, _, weights = pl.dataload(plot['ifile'], plot['variable'], 
                                          plot['dates'], realm=plot['realm_cat'], 
                                          scale=plot['scale'], shift=plot['shift'], 
                                          remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                          seasons=plot['seasons'], datatype=plot['data_type'],
                                          cdostring=plot['cdostring'],
                                          gridweights = True,
                                          external_function=plot['external_function'],
                                          external_function_args=plot['external_function_args'])

    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
    if plot['units']:
        units = plot['units']
    # get data at correct depth
    data = _depth_data(data, depth, plot)

# commented out to prevent floating point errors, see issue 39 on gitlab for details.    
# This is considered non-essential functionality, implementation may be fixed at a later date
#    if plot['sigma'] and plot['data_type'] == 'trends':
#        detrenddata, _, _, _, _, _, _ = pl.dataload(plot['ifile'], plot['variable'],
#                                         plot['dates'], realm=plot['realm_cat'],
#                                         scale=plot['scale'], shift=plot['shift'],
#                                         remapf=plot['remap'], remapgrid=plot['remap_grid'],
#                                         seasons=plot['seasons'], datatype='detrend')
#        detrenddata = _full_depth_data(detrenddata, depth, plot)
#        siggrid = trend_significance(detrenddata, plot['sigma'])
#        cvalues, _ = _trend_units(siggrid, units, plot)
#    else:
#        cvalues = None 
    cvalues = None #in place of the above block 
    dft.filltitle(plot)

    anom = True if plot['divergent'] or plot['data_type'] == 'trends' else False
    if plot['data1']['show_stats']:
        label = stats(plot, data, weights=weights, rmse=False) + '\n' + plot['pycmor_label'] 
    else:
        label = None
    _pcolor(data, plot, anom=anom)
    # make plot    
    pr.worldmap(plot['plot_projection'], lon, lat, data, ax_args=plot['data1']['ax_args'], label=label,
         pcolor_args=plot['data1']['pcolor_args'], cblabel=units, plot=plot, cvalues=cvalues,
         **plot['plot_args'], discrete_colorbar=plot['data1']['discrete_colorbar'])
    
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    plot['units'] = units
    return plot_name

def ttest(data1, data2):
    if data1.ndim == data2.ndim:
        t, p = sp.stats.ttest_ind(data1, data2, axis=0, equal_var=False)
        return p
    else:
        with open('logs/log.txt', 'a') as outfile:
            outfile.write('Significance could not be calculated.\n')        
    return None

def colormap_comparison(plot):
    """ Loads and plots the data for a time averaged map.
        Loads and plots the data for comparison and plots the
        difference between the data and the comparison data.

    Parameters
    ----------
    plot : dictionary
    func : a method that will plot the data on a specified map

    Returns
    -------
    string : name of the plot
    """

    #np.seterr(all='raise')
    print('plotting comparison map of ' + plot['variable'])
    # load data from netcdf file
    data, lon, lat, depth, units, _, weights = pl.dataload(plot['ifile'], plot['variable'], 
                                          plot['dates'], realm=plot['realm_cat'], 
                                          scale=plot['scale'], shift=plot['shift'], 
                                          remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                          seasons=plot['seasons'], datatype=plot['data_type'],
                                          cdostring=plot['cdostring'], gridweights=True,
                                          external_function=plot['external_function'],
                                          external_function_args=plot['external_function_args'])
    data = _depth_data(data, depth, plot)

    data2, _, _, _, _, _, _ = pl.dataload(plot['comp_file'], plot['variable'], 
                                        plot['comp_dates'], realm=plot['realm_cat'], 
                                        scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                        cdostring=plot['cdostring'],
                                        external_function=plot['external_function'],
                                        external_function_args=plot['external_function_args'],
                                        depthneeded=[plot['plot_depth']])
    
    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
        data2, _ = _trend_units(data2, units, plot)
    if plot['units']:
        units = plot['units']

    if plot['alpha'] and plot['data_type'] == 'climatology':

        fulldata, _, _, _, _, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                      plot['dates'], realm=plot['realm_cat'], 
                                      scale=plot['scale'], shift=plot['shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['seasons'], depthneeded=[plot['plot_depth']])
        fulldata2, _, _, _, _, _, _ = pl.dataload(plot['comp_file'], plot['variable'], 
                                        plot['comp_dates'], realm=plot['realm_cat'], 
                                        scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['comp_seasons'], depthneeded=[plot['plot_depth']])    
        pvalues = ttest(fulldata, fulldata2)
    else:
        pvalues = None

# commented out to prevent floating point errors, see issue 39 on gitlab for details.    
# This is considered non-essential functionality, implementation may be fixed at a later date
#    if plot['sigma'] and plot['data_type'] == 'trends':
#        detrenddata, _, _, _, _, _, _ = pl.dataload(plot['ifile'], plot['variable'],
#                                         plot['dates'], realm=plot['realm_cat'],
#                                         scale=plot['scale'], shift=plot['shift'],
#                                         remapf=plot['remap'], remapgrid=plot['remap_grid'],
#                                         seasons=plot['seasons'], datatype='detrend')
#        detrenddata = _full_depth_data(detrenddata, depth, plot)
#        siggrid = trend_significance(detrenddata, plot['sigma'])
#        cvalues, _ = _trend_units(siggrid, units, plot)
#        detrenddata, _, _, _, _, _, _ = pl.dataload(plot['comp_file'], plot['variable'],
#                                         plot['comp_dates'], realm=plot['realm_cat'],
#                                         scale=plot['comp_scale'], shift=plot['comp_shift'],
#                                         remapf=plot['remap'], remapgrid=plot['remap_grid'],
#                                         seasons=plot['comp_seasons'], datatype='detrend')
#        detrenddata = _full_depth_data(detrenddata, depth, plot)
#        siggrid = trend_significance(detrenddata, plot['sigma'])
#        c2values, _ = _trend_units(siggrid, units, plot)        
#    else:
#        cvalues = None 
#        c2values = None
    cvalues = None #these two in place of the above block
    c2values = None
    try:
        compdata = data - data2
    except:
        data2 = data2.transpose()
        compdata = data - data2
    anom = True if plot['divergent'] or plot['data_type'] == 'trends' else False
    _comp_pcolor(data, data2, plot, anom=anom)

    label1 = None
    label2 = None
    label3 = None
    if plot['data1']['show_stats']:
        label1 = stats(plot, data, weights=weights, rmse=False)  + '\n' + plot['pycmor_label']
    if plot['data2']['show_stats']:
        label2 = stats(plot, data2, weights=weights, rmse=False) 
    if plot['comp']['show_stats']:
        label3 = stats(plot, compdata, weights=weights, rmse=True)
    dft.filltitle(plot)
    fig = plt.figure(figsize=(8,12))
    plot_numbers = (1,2,3)
    #if 'polar_map_both' we need 6 plots total instead of 3
    if plot['plot_projection'] == 'polar_map_both':
        plot_numbers = (1,3,5)

    # make plots of data, comparison data, data - comparison data
    pr.worldmap(plot['plot_projection'], lon, lat, data,plot_numbers[0], plot=plot, ax_args=plot['data1']['ax_args'],
         pcolor_args=plot['data1']['pcolor_args'], cblabel=units, cvalues=cvalues, label=label1, discrete_colorbar=plot['data1']['discrete_colorbar'], **plot['plot_args'])
    pr.worldmap(plot['plot_projection'], lon, lat, data2,plot_numbers[1], plot=plot, ax_args=plot['data2']['ax_args'],
         pcolor_args=plot['data2']['pcolor_args'], cblabel=units, cvalues=c2values, label=label2, discrete_colorbar=plot['data2']['discrete_colorbar'], **plot['plot_args'])
    pr.worldmap(plot['plot_projection'], lon, lat, compdata,plot_numbers[2], pvalues=pvalues, alpha=plot['alpha'], anom=True, 
         rmse=True, plot=plot, ax_args=plot['comp']['ax_args'], label=label3,
         pcolor_args=plot['comp']['pcolor_args'], cblabel=units, discrete_colorbar=plot['comp']['discrete_colorbar'], **plot['plot_args'])
    
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    plot['units'] = units
    
    return plot_name


def section(plot):
    """ Loads and plots the data for a time average section map.

    Parameters
    ----------
    plot : dictionary
    func : a method that will plot the data on a specified map

    Returns
    -------
    string : name of the plot
    """
    print('plotting section of ' + plot['variable'])
    
    data, _, lat, depth, units, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                        plot['dates'], realm=plot['realm_cat'], 
                                        scale=plot['scale'], shift=plot['shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['seasons'], datatype=plot['data_type'],
                                        section=True, cdostring=plot['cdostring'],
                                        external_function=plot['external_function'],
                                        external_function_args=plot['external_function_args'])

    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
    if plot['units']:
        units = plot['units']

    dft.filltitle(plot)
    anom = True if plot['divergent'] or plot['data_type'] == 'trends' else False
    _pcolor(data, plot, anom=anom)

    fig = plt.figure(figsize=(10,3))
    gs = gridspec.GridSpec(1, 1, width_ratios=[1, 1])    
    
    pr.section(lat, depth, data, plot=plot, ax=plt.subplot(gs[0, 0]), ax_args=plot['data1']['ax_args'],
         pcolor_args=plot['data1']['pcolor_args'], cblabel=units, discrete_colorbar=plot['data1']['discrete_colorbar'])
    plt.subplot(gs[0, 0]).text(0.62, -0.20, plot['pycmor_label'], horizontalalignment='left', verticalalignment='bottom', fontsize=6, transform=plt.subplot(gs[0, 0]).transAxes)
    plot_name = plotname(plot)

    savefigures(plot_name, **plot)
    plot['units'] = units
    return plot_name

def select_common_levels(plot):
    """Gets the vertical levels from both the model file (plot['ifile']) and 
       the comparison file (plot['comp_file']) and calculates the intersection
       of the two sets. Then uses cdo to make new files for each that only
       contain the common levels. 
    """
    ifile_dataset = Dataset(plot['ifile'], 'r')
    ifile_ncvar = pl._ncvar(ifile_dataset,  plot['variable'])
    ifile_depths = pl._depth(ifile_dataset, ifile_ncvar)
        
    comp_dataset = Dataset(plot['comp_file'], 'r') 
    comp_ncvar = pl._ncvar(comp_dataset,  plot['variable'])
    comp_depths = pl._depth(comp_dataset, comp_ncvar)

    depths_intersection = sorted(list(set(ifile_depths) & set(comp_depths)),reverse=True)
    di_str = pl.depthstring(depths_intersection)
    
    ifile = 'ncstore/depthsel_' + os.path.basename(plot['ifile'])
    comp_file =  'cmipfiles/depthsel_' + os.path.basename(plot['comp_file'])
        
    si = 'cdo sellevel,' + di_str + ' ' + plot['ifile'] + ' ' + ifile 
    os.system(si)
    sc = 'cdo sellevel,' + di_str + ' ' + plot['comp_file'] + ' ' + comp_file 
    os.system(sc)
 
    return ifile, comp_file, depths_intersection    


def section_comparison(plot):
    """ Loads and plots the data for a time averaged section map.
        Loads and plots the data for comparison and plots the
        difference between the data and the comparison data.

    Parameters
    ----------
    plot : dictionary
    func : a method that will plot the data on a specified map

    Returns
    -------
    string : name of the plot
    """
    print('plotting section comparison of ' + plot['variable'])
    if plot['use_common_levels']:
        ifile, comp_file, levels_used = select_common_levels(plot)
    else:
       ifile     = plot['ifile'] 
       comp_file = plot['comp_file']     
    
    data2, _, _, depth, _, _, _ = pl.dataload(comp_file, plot['variable'], 
                                        plot['comp_dates'], realm=plot['realm_cat'], 
                                        scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                        section=True, cdostring=plot['cdostring'],
                                        external_function=plot['external_function'],
                                        external_function_args=plot['external_function_args'])
    data, _, lat, depth, units, _, _ = pl.dataload(ifile, plot['variable'], 
                                        plot['dates'], realm=plot['realm_cat'], 
                                        scale=plot['scale'], shift=plot['shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['seasons'], datatype=plot['data_type'],
                                        section=True, cdostring=plot['cdostring'],
                                        external_function=plot['external_function'],
                                        external_function_args=plot['external_function_args'], 
                                        depthneeded=list(depth))
    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
        data2, _ = _trend_units(data2, units, plot)
    if plot['units']:
        units = plot['units']
        
    compdata = data - data2
    dft.filltitle(plot)
    anom = True if plot['divergent'] or plot['data_type'] == 'trends' else False
    _comp_pcolor(data, data2, plot, anom=anom)

    if plot['alpha'] and plot['data_type'] == 'climatology':
        fulldata, _, _, _, _, _, _ = pl.dataload(ifile, plot['variable'], 
                                      plot['dates'], realm=plot['realm_cat'], 
                                      scale=plot['scale'], shift=plot['shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['seasons'], depthneeded=list(depth),
                                      section=True)
        fulldata2, _, _, _, _, _, _ = pl.dataload(comp_file, plot['variable'], 
                                        plot['comp_dates'], realm=plot['realm_cat'], 
                                        scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['comp_seasons'], depthneeded=list(depth),
                                        section=True)
        pvalues = ttest(fulldata, fulldata2)
    else:
        pvalues = None

    # make plots of data, comparison data, data - comparison data
    fig = plt.figure(figsize=(6, 8))
    #gs = gridspec.GridSpec(3, 2, width_ratios=[20, 1])
    pr.section(lat, depth, data, plot=plot, ax=plt.subplot(3,1,1), ax_args=plot['data1']['ax_args'],
         pcolor_args=plot['data1']['pcolor_args'], cblabel=units, discrete_colorbar=plot['data1']['discrete_colorbar'])
    pr.section(lat, depth, data2, plot=plot, ax=plt.subplot(3,1,2), ax_args=plot['data2']['ax_args'],
         pcolor_args=plot['data2']['pcolor_args'], cblabel=units, discrete_colorbar=plot['data2']['discrete_colorbar'])
    pr.section(lat, depth, compdata, anom=True, rmse=True, pvalues=pvalues, 
         alpha=plot['alpha'], plot=plot, ax=plt.subplot(3,1,3), ax_args=plot['comp']['ax_args'],
         pcolor_args=plot['comp']['pcolor_args'], cblabel=units, discrete_colorbar=plot['comp']['discrete_colorbar'])
    
    if plot['use_common_levels']:
        factor = 1
        if plot['vertical_dim_units'] == 'Pa':
            factor = 100
        fig.text(0.1, -0.01,textwrap.fill('only common levels used ('+', '.join([str(lvl/factor) for lvl in levels_used])+')',100), horizontalalignment='left', verticalalignment='top', fontsize=6)
    plt.tight_layout()
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    plot['units'] = units
    return plot_name


def _trend_units(data, units, plot):
    """ Multiplies the data by a scalar factor based on the frequency

    Parameters
    ----------
    data : numpy array
    units : string
    plot : dictionary

    Returns
    -------
    numpy array
    string
    """
    if plot['frequency'] == 'yr' or plot['yearmean']:
        data = data * 10        
    elif plot['frequency'] == 'day':
        data = data * 3650
        data = data * (len(plot['seasons']) / 4)
    elif plot['frequency'] == 'mon':
        data = data * 120
        data = data * (len(plot['seasons']) / 4)
    units = units + '/decade'
    return data, units

def trend_significance(residuals, sigma=0.05):
    nt = len(residuals)
    count = 0
    x = len(residuals[0, :, 0])
    y = len(residuals[0, 0, :])
    rcorrs = np.empty(shape=[x, y])
    for (i,j), value in np.ndenumerate(rcorrs):
        count += 1
        r_corr,_ = sp.stats.pearsonr(residuals[: -1, i, j], residuals[1:, i, j])
        if r_corr < 0:
            r_corr = 0
        rcorrs[i][j] = r_corr
    
    cs = np.empty(shape=[x, y])    
    for (i,j), rcor in np.ndenumerate(rcorrs):
        neff = float(nt * (1-rcor) / (1 + rcor))
        #neff = nt
        a = residuals[:,i,j]
        b = a * a
        d = sum(b)
        se = np.sqrt( d / ( neff - 2 ) )
        sb = se / np.sqrt( sum( ( np.arange(nt) - np.mean( np.arange(nt) ) )**2 ) )

        tcrit = sp.stats.t.isf(sigma/2.0, nt - 2 )

        c = tcrit * sb

        cs[i][j] = c
    return cs

def _histogram_data(plot, compfile):
    data, _, _, _, _, _, _ = pl.dataload(compfile, plot['variable'], 
                              plot['comp_dates'], realm=plot['realm_cat'], 
                              scale=plot['comp_scale'], shift=plot['comp_shift'], 
                              remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                              seasons=plot['comp_seasons'], datatype=plot['data_type'],
                              yearmean=plot['yearmean'],
                              fieldmean=True, cdostring=plot['cdostring'],
                              external_function=plot['external_function'],
                              external_function_args=plot['external_function_args'],
                              depthneeded=plot['plot_depth'])
    if plot['data_type'] == 'trends':
        data, _ = _trend_units(data, '', plot)
    return data

def histogram(plot):
    values = {}
    data, _, _, depth, units, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                              plot['comp_dates'], realm=plot['realm_cat'], 
                              scale=plot['comp_scale'], shift=plot['comp_shift'], 
                              remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                              seasons=plot['comp_seasons'], datatype=plot['data_type'],
                              yearmean=plot['yearmean'],
                              cdostring=plot['cdostring'], fieldmean=True,
                              external_function=plot['external_function'],
                              external_function_args=plot['external_function_args'])
    
    data = _1d_depth_data(data, depth, plot)
    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
    
    if plot['units']:
        units = plot['units']
    values = []
    
    dft.filltitle(plot)
    
    obs_colors = mpl.cm.Blues(np.linspace(0.6,1,len(plot['comp_obs'])))
    for o,c in zip(plot['comp_obs'],obs_colors):
        values.append({'name': o,
                       'data': _histogram_data(plot, plot['obs_file'][o]),
                       'color': c})

    id_colors = mpl.cm.YlOrBr(np.linspace(0.6,1,len(plot['comp_ids'])))
    for i,c in zip(plot['comp_ids'],id_colors):
        values.append({'name': i, 
                       'data': _histogram_data(plot, plot['id_file'][i]),
                       'color': c})

    mdl_colors = mpl.cm.Greens(np.linspace(0.6,1,len(plot['comp_models'])))
    for m,c in zip(plot['comp_models'],mdl_colors):
        for realization_file in plot['model_files'][m]:
            values.append({'name': m,
                           'data': _histogram_data(plot, realization_file),
                           'color': c})

    # if there are ensemble members plot these, otherwise just plot the single realization   
    if plot['ensemble_files']:
        ens_color = 'lightcoral'
        for e in plot['ensemble_files']:
            values.append({'name': plot['model_ID'],
                           'data': _histogram_data(plot, plot['ensemble_files'][e]),
                           'color': ens_color})
    else:
        values.append({'name': plot['model_ID'],
                       'data': data,
                       'color': 'r'})
 
    cmipdata = []
    for f in plot['cmip5_files']:
        try:
            cmipdata.append(_histogram_data(plot, f))
        except:
            continue
    
    
    plot['data1']['ax_args']['xlabel'] = 'Trends ' + plot['comp_dates']['start_date'] + ' to ' + plot['comp_dates']['end_date'] + ' (' + units + ')'
    plot['data1']['ax_args']['ylabel'] = '# Realizations'
    pr.histogram(cmipdata, values, ax_args=plot['data1']['ax_args'], plot=plot)
    if plot['data1']['footnote'] is not None:
         plt.figtext(0.01, 0.01, plot['data1']['footnote'], horizontalalignment='left')
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    plot['units'] = units
    return plot_name
    
def _timeseries_data(plot, compfile):
    data, _, _, _, _, time, _ = pl.dataload(compfile, plot['variable'], 
                                         plot['comp_dates'], realm=plot['realm_cat'], 
                                         scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                         remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                         seasons=plot['comp_seasons'], fieldmean=True,
                                         cdostring=plot['cdostring'],
                                         yearmean=plot['yearmean'],
                                         external_function=plot['external_function'],
                                         external_function_args=plot['external_function_args'],
                                         depthneeded=[plot['plot_depth']])

    return data, time


def timeseries(plot):
    print('plotting timeseries comparison of ' + plot['variable'])
    data, _, _, depth, units, time, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                         plot['dates'], realm=plot['realm_cat'], 
                                         scale=plot['scale'], shift=plot['shift'], 
                                         remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                         seasons=plot['seasons'], fieldmean=True,
                                         yearmean=plot['yearmean'],
                                         cdostring=plot['cdostring'],
                                         external_function=plot['external_function'],
                                         external_function_args=plot['external_function_args'])
    
    plot['data1']['ax_args']['xlabel'] = 'Time'
    if 'ylabel' not in plot['data1']['ax_args']:
        if plot['units']:
            plot['data1']['ax_args']['ylabel'] = plot['units']
        else:
            plot['data1']['ax_args']['ylabel'] = units
            plot['units'] = units
    plot['plot_depth'] = None
    if data.ndim > 1:
        plot['plot_depth'] = min(depth, key=lambda x: abs(x - plot['depth']))

        try:
            depth_ind = np.where(np.round(depth) == plot['plot_depth'])[0][0]
        except:
            print(('Failed to extract depth ' + plot['plot_depth'] + ' for ' + plot['variable']))
            depth_ind = 0
        data = data[:, depth_ind]

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    dft.filltitle(plot)
    
    # make plot
    handles = []
    # plot comparison data on the same axis
    if plot['cmip5_file']:
        plot['comp_model'] = 'cmip5'
        data_c, x = _timeseries_data(plot, plot['cmip5_file'])
        pr.timeseries(x, data_c, plot=plot, ax=ax, label=plot['comp_model'], ax_args=plot['data1']['ax_args'], color='k', zorder=5)
        handles.append(mpatches.Patch(color='k', label=str(plot['comp_model'])))
    obs_colors = mpl.cm.Blues(np.linspace(0.6,1,len(plot['comp_obs'])))
    for o,c in zip(plot['comp_obs'],obs_colors):
        plot['comp_model'] = o
        data_o, x = _timeseries_data(plot, plot['obs_file'][o])
        pr.timeseries(x, data_o, plot=plot, ax=ax, label=plot['comp_model'], ax_args=plot['data1']['ax_args'], color=c, zorder=6)
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))
    mdl_colors = mpl.cm.Greens(np.linspace(0.6,1,len(plot['comp_models'])))
    for model,c in zip(plot['comp_models'],mdl_colors):
        plot['comp_model'] = model
        for realization_file in plot['model_files'][model]:
            data_m, x = _timeseries_data(plot, realization_file)
            pr.timeseries(x, data_m, plot=plot, ax=ax, label=plot['comp_model'], ax_args=plot['data1']['ax_args'], color=c, zorder=3)
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))
    id_colors = mpl.cm.YlOrBr(np.linspace(0.6,1,len(plot['comp_ids'])))
    for i,c in zip(plot['comp_ids'],id_colors):
        plot['comp_model'] = i
        data_i, x = timeseriesdata(plot, plot['id_file'][i], depth)
        pr.timeseries(x, data_i, plot=plot, ax=ax, label=plot['comp_model'], ax_args=plot['data1']['ax_args'], color=c, zorder=4)
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))

    # if there are ensemble members plot these, otherwise just plot the single realization
    if plot['ensemble_files']:
        ens_color = 'lightcoral'
        handles.append(mpatches.Patch(color=ens_color, label=plot['model_ID']))
        for e in plot['ensemble_files']:
            plot['comp_model'] = e
            data_e, x = _timeseries_data(plot, plot['ensemble_files'][e])
            pr.timeseries(x, data_e, plot=plot, ax=ax, label=plot['comp_model'], ax_args=plot['data1']['ax_args'], color=ens_color,  zorder=2)
    else:
        pr.timeseries(time, data, plot=plot, ax=ax, label=plot['model_ID'], ax_args=plot['data1']['ax_args'], color='r', zorder=6)
        handles.append(mpatches.Patch(color='r', label=plot['model_ID'])) 
    
    #plot cmips
    if plot['cmip5_files']:
        handles.append(mpatches.Patch(color='0.75', label='cmips')) 
        for f in plot['cmip5_files']:
            try:
                plot['comp_model'] = 'cmip'
                data, x = _timeseries_data(plot, f)
                pr.timeseries(x, data, plot=plot, ax=ax, label=None, ax_args=plot['data1']['ax_args'], color='0.75', zorder=1)
            except:
                continue

    ax.legend(handles=handles, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.yaxis.set_major_formatter(ticker.ScalarFormatter(useOffset=False))
    if plot['data1']['footnote'] is not None:
         plt.figtext(0.01, 0.01, plot['data1']['footnote'], horizontalalignment='left')
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    return plot_name

def zonalmeandata(plot, compfile):

    data, _, _, _, _, _, _ = pl.dataload(compfile, plot['variable'], 
                                  plot['comp_dates'], realm=plot['realm_cat'], 
                                  scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                  remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                  seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                  section=True, 
                                  external_function=plot['external_function'],
                                  external_function_args=plot['external_function_args'],
                                  depthneeded=[plot['plot_depth']])
    return data

def zonalmean(plot):
    """ Loads and plots a time average of the zonal means
        for each latitude. Loads and plots the data for comparison.

    Parameters
    ----------
    plot : dictionary
    func : a method that will plot the data on a specified map

    Returns
    -------
    string : name of the plot
    """
    print('plotting zonal mean of ' + plot['variable'])
    data, _, lat, depth, units, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                        plot['dates'], realm=plot['realm_cat'], 
                                        scale=plot['scale'], shift=plot['shift'], 
                                        remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                        seasons=plot['seasons'], datatype=plot['data_type'],
                                        section=True,
                                        external_function=plot['external_function'],
                                        external_function_args=plot['external_function_args'])

    if 'ylabel' not in plot['data1']['ax_args']:
        if plot['units']:
            plot['data1']['ax_args']['ylabel'] = plot['units']
        else:
            plot['data1']['ax_args']['ylabel'] = units
            plot['units'] = units
    # get data at the correct depth 
    plot['plot_depth'] = None
    if data.ndim > 1:
        plot['plot_depth'] = min(depth, key=lambda x: abs(x - plot['depth']))
        try:
            depth_ind = np.where(np.round(depth) == plot['plot_depth'])[0][0]
        except:
            print(('Failed to extract depth ' + plot['plot_depth'] + ' for ' + plot['variable']))
            depth_ind = 0
        data = data[depth_ind, :]

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    dft.filltitle(plot)
   
    # make plot
    # plot comparison data on the same axis
    handles = []
    if plot['cmip5_file']:
        plot['comp_model'] = 'cmip5'
        data_c = zonalmeandata(plot, plot['cmip5_file'])
        pr.zonalmean(lat, data_c, plot=plot, ax=ax, label=plot['comp_model'], color='k', zorder=5)
        handles.append(mpatches.Patch(color='k', label='cmip5')) 
    obs_colors = mpl.cm.Blues(np.linspace(0.6,1,len(plot['comp_obs'])))
    for o,c in zip(plot['comp_obs'],obs_colors):
        plot['comp_model'] = o
        data_o = zonalmeandata(plot, plot['obs_file'][o])
        pr.zonalmean(lat, data_o, plot=plot, ax=ax, label=plot['comp_model'], color=c, zorder=6)
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))
    mdl_colors = mpl.cm.Greens(np.linspace(0.6,1,len(plot['comp_models'])))
    for m,c in zip(plot['comp_models'],mdl_colors):
        plot['comp_model'] = m
        for realization_file in plot['model_files'][m]:
            data_m = zonalmeandata(plot, realization_file)
            pr.zonalmean(lat, data_m, plot=plot, ax=ax, label=plot['comp_model'], color=c, zorder=3)        
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))
    id_colors = mpl.cm.YlOrBr(np.linspace(0.6,1,len(plot['comp_ids'])))
    for i,c in zip(plot['comp_ids'],id_colors):
        plot['comp_model'] = i
        data_i = zonalmeandata(plot, plot['id_file'][i])
        pr.zonalmean(lat, data_i, plot=plot, ax=ax, label=plot['comp_model'], color=c, zorder=4)
        handles.append(mpatches.Patch(color=c, label=str(plot['comp_model'])))

    # if there are ensemble members plot these, otherwise just plot the single realization   
    if plot['ensemble_files']:
        ens_color = 'lightcoral'
        handles.append(mpatches.Patch(color=ens_color, label=plot['model_ID']))
        for e in plot['ensemble_files']:
            plot['comp_model'] = e
            data_e = zonalmeandata(plot, plot['ensemble_files'][e])
            pr.zonalmean(lat, data_e, plot=plot, ax=ax, ax_args=plot['data1']['ax_args'], label=plot['comp_model'], color=ens_color, zorder=2)
    else:
        pr.zonalmean(lat, data, plot=plot, ax=ax, ax_args=plot['data1']['ax_args'], color='r', zorder=6)
        handles.append(mpatches.Patch(color='r', label=plot['model_ID'])) 
    #plot cmips
    if plot['cmip5_files']:
        handles.append(mpatches.Patch(color='0.75', label='cmips')) 
        for f in plot['cmip5_files']:
            try:
                plot['comp_model'] = 'cmip'
                data = zonalmeandata(plot, f)
                pr.zonalmean(lat, data, plot=plot, ax=ax, color='0.75', zorder=1)
            except:
                continue
 
    ax.legend(handles=handles, loc='center left', bbox_to_anchor=(1, 0.5))
    if plot['data1']['footnote'] is not None:
         plt.figtext(0.01, 0.01, plot['data1']['footnote'], horizontalalignment='left')
    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    return plot_name

def weighted_std(data, weights=None):
    if weights is None:
        weights = np.ones(data.shape)

    flattened_data = data.flatten()
    flattened_weights = weights.flatten()    

    mean = np.ma.average(flattened_data, weights=flattened_weights)
    std = np.sqrt(np.ma.average((flattened_data-mean)**2, weights=flattened_weights))
    
    return std
    
def weighted_correlation(obs, data, weights=None):
    """Compute a weighted correlation coefficient and std dev for obs and model

    Returns:
       r : correlation coefficient
       ovar : weighted stddev of obs
       dvar : weighted stddev of data

    """

    if weights is None:
        print('weights are None')
        weights = np.ones(obs.shape)
    new_order = [obs.shape.index(i) for i in data.shape]
    redata = data.transpose(new_order)
    new_order = [obs.shape.index(i) for i in weights.shape]
    reweights = weights.transpose(new_order)    
#    redata = data.reshape(obs.shape)
#    reweights = weights.reshape(obs.shape)
    
    obsf = obs#.flatten()
    
    dataf = redata#.flatten()
    weightsf = reweights#.flatten()

    obar = np.ma.average(obsf, weights=weightsf)
    dbar = np.ma.average(dataf, weights=weightsf)
    ovar = np.sqrt(np.ma.average((obsf-obar)**2, weights=weightsf))
    dvar = np.sqrt(np.ma.average((dataf-dbar)**2, weights=weightsf))
    r =  np.ma.average((obsf-obar)*(dataf-dbar), weights=weightsf) / (ovar*dvar)

    return r, ovar, dvar

def taylor_load(plot, compfile, depth, i, color, refdata, weights):
    data, _, _, _, _, _, _ = pl.dataload(compfile, plot['variable'], 
                                      plot['comp_dates'], realm=plot['realm_cat'], 
                                      scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=depth)
    corr, refstd, std = weighted_correlation(refdata, data, weights)
    stats_dictionary(plot, compfile, depth, std, std / refstd, corr)
    
    return {'name': plot['comp_model'],
            'corrcoef': corr,
            'std': std / refstd,
            'color': color,
            'marker': i,
            'zorder': 2}

def stats_dictionary(plot, filename, depth, sd, nsd, corrcoef):
    if filename not in plot['stats']:
        plot['stats'][filename] = {}

    try: 
        plot['stats'][filename][depth] = {'standard deviation': float(sd),
                                      'normalized standard deviation': float(nsd),
                                      'correlation coefficient': float(corrcoef)}
    except:
        plot['stats'][filename] = {'standard deviation': float(sd),
                                      'normalized standard deviation': float(nsd),
                                      'correlation coefficient': float(corrcoef)}        
                                      
#    plot['stats'] = {'obserations': {'standard deviation': float(refstd)}}
          
def taylor(plot):
    # build list of specified datasets that respect the order set by the user
    #   and set the first item to be the references data
    obs_lst = [ o for o in plot['comp_obs'] if o in list(plot['obs_file'].keys()) ]
    ref_obs = obs_lst[0]

    labelled_stats = []
    unlabelled_stats = []
    plot['plot_depth'] = plot['depths'][0]
    plot['stats'] = {}
 
    dft.filltitle(plot)
    
    for i, d in enumerate(plot['depths']):
        refdata, _, _, depth, units, _, weights = pl.dataload(plot['obs_file'][ref_obs], plot['variable'], 
                                      plot['comp_dates'], realm=plot['realm_cat'], 
                                      scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                      gridweights=True,
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=[d])

        refstd = weighted_std(refdata, weights)
        stats_dictionary(plot, ref_obs, d, refstd, 1, 1)

        data, _, _, _, _, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                      plot['comp_dates'], realm=plot['realm_cat'], 
                                      scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=depth)
        corrcoef, _, std = weighted_correlation(refdata, data, weights)
        stats_dictionary(plot, plot['ifile'], d, std, std / refstd, corrcoef)
        if plot['cmip5_file']:
            plot['comp_model'] = 'cmip5'
            labelled_stats.append(taylor_load(plot, plot['cmip5_file'], depth, '$%d$' % (i+1), 'k', refdata, weights))

        obs_colors = mpl.cm.Blues(np.linspace(0.6,0.9,len(obs_lst[1:])))
        for o,c in zip(obs_lst[1:],obs_colors): # skip first entry as it is the reference
            plot['comp_model'] = o
            labelled_stats.append(taylor_load(plot,plot['obs_file'][o], depth, '$%d$' % (i+1), c, refdata,weights))
        mdl_colors = mpl.cm.Greens(np.linspace(0.6,1,len(plot['comp_models'])))
        for m,c in zip(plot['comp_models'],mdl_colors):
            plot['comp_model'] = m
            for realization_file in plot['model_files'][m]:
                labelled_stats.append(taylor_load(plot, realization_file, depth, '$%d$' % (i+1), c, refdata, weights))
        id_colors = mpl.cm.YlOrBr(np.linspace(0.6,1,len(plot['comp_ids'])))
        for cid,c in zip(plot['comp_ids'],id_colors):
            plot['comp_model'] = cid
            labelled_stats.append(taylor_load(plot, plot['id_file'][cid], depth, '$%d$' % (i+1), c, refdata, weights))
 

        # if there are ensemble members plot these, otherwise just plot the single realization   
        if plot['ensemble_files']:
            ens_color = 'lightcoral'
            for e in plot['ensemble_files']:
                plot['comp_model'] = plot['model_ID']
                labelled_stats.append(taylor_load(plot,plot['ensemble_files'][e], depth, '$%d$' % (i+1), ens_color, refdata,weights))
        else:
            labelled_stats.append({'name': plot['model_ID'],
                                   'corrcoef': corrcoef,
                                   'std': std / refstd,
                                   'color': 'red',
                                   'marker': '$%d$' % (i+1),
                                   'zorder': 3})
 




        for f in plot['cmip5_files']:
            plot['comp_model'] = 'cmip'
            try:
                unlabelled_stats.append(taylor_load(plot, f, depth, '$%d$' % (i+1), '0.75', refdata, weights))     
            except:
                continue
    
    depthlist = [str(i + 1) + ': ' + str(d) for i, d in enumerate(plot['depths'])] 
    label = '  '.join(depthlist)

    if len(plot['depths']) <= 1:
        for l in labelled_stats:
            l['marker'] = '.'
        for l in unlabelled_stats:
            l['marker'] = '.'
        label = None
    
    pr.taylor_from_stats(labelled_stats, unlabelled_stats, obs_label=ref_obs,
                         label=label, ax_args=plot['data1']['ax_args'])
#    plot['stats'] = {'obserations': {'standard deviation': float(refstd)}}
    if plot['data1']['footnote'] is not None:
         plt.figtext(0.01, 0.01, plot['data1']['footnote'], horizontalalignment='left')
    plot_name = plotname(plot)
    #plt.tight_layout()
    savefigures(plot_name, **plot)
    if not plot['units']:
        plot['units'] = units
    plot['comp_file'] = plot['obs_file']
    return plot_name

def multivariable_taylor(plot):
    if 'depth' not in plot:
        plot['depth'] = 0
    labelled_stats = []    
    obs = next(iter(plot['obs_file'].keys()))
    plot['stats'] = {}
    colors = plt.matplotlib.cm.jet(np.linspace(0,1,len(plot['extra_variables']) + 1))
    refdata, _, _, depth, units, _, weights = pl.dataload(plot['obs_file'][obs], plot['variable'], 
                                      plot['comp_dates'], realm=plot['realm_cat'], 
                                      scale=plot['comp_scale'], shift=plot['comp_shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                      gridweights=True,
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=[plot['depth']])

    refstd = weighted_std(refdata, weights)

    data, _, _, _, _, _, _ = pl.dataload(plot['ifile'], plot['variable'], 
                                      plot['dates'], realm=plot['realm_cat'], 
                                      scale=plot['scale'], shift=plot['shift'], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['seasons'], datatype=plot['data_type'],
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=[plot['depth']])
    corrcoef, _, std = weighted_correlation(refdata, data, weights)
    labelled_stats.append({'name': plot['variable'],
                           'corrcoef': corrcoef,
                           'std': std / refstd,
                           'color': colors[0],
                           'marker': '.',
                           'zorder': 3})
    stats_dictionary(plot, plot['ifile'], plot['depth'], std, std / refstd, corrcoef)    
    for i, var in enumerate(plot['extra_variables']):
        refdata, _, _, depth, units, _, weights = pl.dataload(plot['extra_obs_files'][var], var, 
                                      plot['comp_dates'], realm=plot['extra_realm_cats'][var], 
                                      scale=plot['extra_comp_scales'][i], shift=plot['extra_comp_shifts'][i], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['comp_seasons'], datatype=plot['data_type'],
                                      gridweights=True,
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=[plot['depth']])            
    
        refstd = weighted_std(refdata, weights)

        data, _, _, _, _, _, _ = pl.dataload(plot['extra_ifiles'][var], var, 
                                      plot['dates'], realm=plot['realm_cat'], 
                                      scale=plot['extra_scales'][i], shift=plot['extra_shifts'][i], 
                                      remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                      seasons=plot['seasons'], datatype=plot['data_type'],
                                      external_function=plot['external_function'],
                                      external_function_args=plot['external_function_args'],
                                      depthneeded=[plot['depth']])

        corrcoef, _, std = weighted_correlation(refdata, data, weights)
        labelled_stats.append({'name': var,
                               'corrcoef': corrcoef,
                               'std': std / refstd,
                               'color': colors[i + 1],
                               'marker': '.',
                               'zorder': 3})
        stats_dictionary(plot, plot['extra_ifiles'][var], plot['depth'], std, std / refstd, corrcoef)    
    if not plot['data1']['title_flag']:
        plot['data1']['ax_args']['title'] = plot['data_type'] + ' ' + plot['model_ID'] + ' ' + plot['dates']['start_date'] + ' - ' + plot['dates']['end_date']  
    pr.taylor_from_stats(labelled_stats, [], obs_label='observations',
                         label=None, ax_args=plot['data1']['ax_args'])
    plot_name = plotname(plot)
    plt.tight_layout()
    savefigures(plot_name, **plot)
    if not plot['units']:
        plot['units'] = '--'
    plot['comp_file'] = plot['obs_file']
    return plot_name
    
def scatter(plot):
    print('plotting scatter map of ' + plot['variable'] + ' and ' + plot['extra_variables'][0])
    # load data from netcdf file
    data, lon, lat, depth, units, _, weights = pl.dataload(plot['ifile'], plot['variable'], 
                                          plot['dates'], realm=plot['realm_cat'], 
                                          scale=plot['scale'], shift=plot['shift'], 
                                          remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                          seasons=plot['seasons'], datatype=plot['data_type'],
                                          cdostring=plot['cdostring'],
                                          external_function=plot['external_function'],
                                          external_function_args=plot['external_function_args'])
    # get data at correct depth
    data = _depth_data(data, depth, plot)
    data2, _, _, _, units2, _, _ = pl.dataload(plot['extra_ifiles'][plot['extra_variables'][0]], 
                                          plot['extra_variables'][0], 
                                          plot['dates'], 
                                          realm=plot['extra_realm_cats'][plot['extra_variables'][0]], 
                                          scale=plot['extra_scales'][0], shift=plot['shift'], 
                                          remapf=plot['remap'], remapgrid=plot['remap_grid'], 
                                          seasons=plot['seasons'], datatype=plot['data_type'],
                                          cdostring=plot['cdostring'],
                                          external_function=plot['external_function'],
                                          external_function_args=plot['external_function_args'],
                                          depthneeded=[plot['plot_depth']])
    if plot['data_type'] == 'trends':
        data, units = _trend_units(data, units, plot)
        data2, units2 = _trend_units(data2, units2, plot)
    if plot['units']:
        units = plot['units']
    if not plot['data1']['title_flag']:
        plot['data1']['ax_args']['title'] = plot['data_type'] + ' ' + plot['model_ID'] + ' ' + plot['dates']['start_date'] + ' - ' + plot['dates']['end_date']
    if 'xlabel' not in plot['data1']['ax_args']:
        plot['data1']['ax_args']['xlabel'] = plot['variable'] + ' ' + units
    if 'ylabel' not in plot['data1']['ax_args']:
        plot['data1']['ax_args']['ylabel'] = plot['extra_variables'][0] + ' ' + units2
    

    # make plot    
    pr.scatter(data, data2, ax_args=plot['data1']['ax_args'], plot=plot)

    plot_name = plotname(plot)
    savefigures(plot_name, **plot)
    plot['units'] = units
    return plot_name
       
