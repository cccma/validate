"""
directory_tools
===============

This module contains several functions associated with traversing
directories and gathering information from files.

.. moduleauthor:: David Fallis
"""
import os, datetime, itertools, tarfile, traceback, shutil, getpass
from netCDF4 import Dataset, num2date, date2num
import cmipdata as cd
from pprint import pprint
from collections import defaultdict
import cdo
cdo = cdo.Cdo()
import time

#MEANDIR = None

def _variable_dictionary(plots):
    """ Creates a dictionary with the variable names as keys
        mapped to empty lists

    Parameters
    ----------
    plots : list of dictionaries with 'variable' key

    Returns
    -------
    dictionary
    """
    variables = {}
    for p in plots:
        variables[p['variable']] = []
        for evar in p['extra_variables']:
            variables[evar] = []
    return variables


def min_start_dates(plots):
    """ Returns a dictionary which maps the variable names
        to the earliest year needed for that variable in all of the plots

    Parameters
    ----------
    plots : list of dictionaries

    Returns
    -------
    dictionary
    """
    start_dates = _variable_dictionary(plots)
    for p in plots:
        try:
            start_dates[p['variable']].append(p['dates']['start_date'])
        except: pass
        try:
            start_dates[p['variable']].append(p['comp_dates']['start_date'])
        except: pass
        
    for var in start_dates:
        start_dates[var] = [int(date[:4]) for date in start_dates[var]]
        try:
            start_dates[var] = min(start_dates[var])
        except:
            start_dates[var] = None
    return start_dates

def max_end_dates(plots):
    """ Returns a dictionary which maps the variable names
        to the latest year needed for that variable in all of the plots

    Parameters
    ----------
    plots : list of dictionaries

    Returns
    -------
    dictionary
    """
    end_dates = _variable_dictionary(plots)
    for p in plots:
        try:
            end_dates[p['variable']].append(p['dates']['end_date'])
        except: pass
        try:
            end_dates[p['variable']].append(p['comp_dates']['end_date'])
        except: pass

    for var in end_dates:
        end_dates[var] = [int(date[:4]) for date in end_dates[var]]
        try:
            end_dates[var] = max(end_dates[var])
        except:
            end_dates[var] = None
    return end_dates


def traverse(root):
    """ Returns a list of all filenames including the path
        within a directory or any subdirectories.

        Note: ignores 'hidden' files.

    Parameters
    ----------
    root : string
           directory path

    Returns
    -------
    list of strings

    """
    files = []
    for dirname, subdirlist, filelist in os.walk(root):
        for f in filelist:
            if not f[0] == '.':
               files.append(dirname + '/' + f)
    return files


def _mkdir():
    """ Tries to make directories used to store processed *.nc files
    """
    try:
        os.makedirs('ncstore')
    except:
        try:
            os.system('rm ncstore/*.nc')
        except:
            pass

    def mkthedir(name):
        try:
            os.makedirs(name)
        except:
            pass
    mkthedir('mask')
    mkthedir('plots')
    mkthedir('logs')
    mkthedir('netcdf')
    mkthedir('cmipfiles')


def _logfile(run, experiment):
    with open('logs/log.txt', 'w') as outfile:
        outfile.write('Run ID: ' + run + '\n')
        outfile.write('Experiment: ' + experiment + '\n\n')

    with open('logs/log.yml', 'w') as outfile:
        outfile.write('Run ID: ' + run + '\n')
        outfile.write('Experiment: ' + experiment + '\n\n')

def _load_masks(files):
    """Loads the land and sea masks for a specified run

    Parameters
    ----------
    files : list
            file names
    """
    for f in files:
        var = getvariable(f)
        if var == 'sftof':
            print('found ocean')
            os.system('ln -s ' + f + ' ./mask/ocean')
        if var == 'sftlf':
            os.system('ln -s ' + f + ' ./mask/land')
            print('found land')
        fxvars = ['areacello',
                  'basin',
                  'deptho',
                  'thkcello',
                  'volcello',
                  'areacella',
                  'orog',
                  'orograw',
                  'mrsofc',
                  'rootd',
                  'sftgif',
                  ]
        if var in fxvars:
            print('found ' + var)
            os.system('ln -s ' + f + ' ./mask/' + var)          

def _remove_files_out_of_date_range(filedict, start_dates, end_dates):
    """ Removes file names from a dictionary which will not be needed because
        they are outside the date range

    Parameters
    ----------
    filedict : dictionary
               maps tuple to a list of file names
    start_dates : dictionary
                  maps variable name to ealiest year needed
    end_dates : dictionary
                maps variable name to latest year needed

    Returns
    -------
    dictionary with some file names removed
    """
    for d in filedict:
        if len(filedict[d]) > 1:
            for infile in filedict[d][:]:
                sd, ed = getdates(infile)
                if end_dates[d[1]]:
                    if int(sd) > int(end_dates[d[1]]):
                        filedict[d].remove(infile)
                if start_dates[d[1]]:
                    if int(ed) < int(start_dates[d[1]]):
                        try:
                            filedict[d].remove(infile)
                        except:
                            pass
    return filedict


def _cat_file_slices(filedict):
    """ Catenates the list of files under each key
        the dictionary now maps to the new filename

    Parameters
    ----------
    filedict : dictionary
               maps tuple to a list of file names

    Returns
    -------
    dictionary mapping to new file name
    """
    count = 0
    for d in filedict:
        if len(filedict[d]) > 1:
            count += 1
            outfile = 'ncstore/merged' + filedict[d][0].rsplit('/', 1)[1]
            infiles = ' '.join(filedict[d])
            os.system('cdo mergetime ' + infiles + ' ' + outfile)
            filedict[d] = (outfile)
        else:
            filedict[d] = filedict[d][0]
    return filedict

# 2020 cleanup NCS: Many of the functions below open a file to query it's metatdata. When this is done on thousands of files without consideration it is unacceptably expensive.\
# 2020 cleanup NCS: Required metadata should ideally be extracted once, into a database, and referneced from there on out.

def getdates(f):
    """ Returns the years from a filename and directory path

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of start year
    string of end year
    """
    nc = Dataset(f, 'r')
    time = nc.variables['time_bnds'][:].squeeze()
    nc_time = nc.variables['time']
    try:
        cal = nc_time.calendar
    except:
        cal = 'standard'
    start = nc_time[:][0]
    end = nc_time[:][-1]
    start = num2date(start, nc_time.units, cal)
    end = num2date(end, nc_time.units, cal)
    start = start.year
    end = end.year
    return start, end


def getvariable(f):
    """ Returns the variable from a filename and directory path

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of var
    """
    x = f.rsplit('/', 1)
    x = x[1].split('_', 1)
    return x[0]


def getfrequency(f):
    """ Returns the frequency from a filename and directory path.
        ex. 'day', 'mon', 'yr', 'fx'
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of frequency
    """
    nc = Dataset(f, 'r')
    return str(nc.__getattribute__('frequency'))


def getpycmorhash(f):
    """ Returns the pycmor hash from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of pycmor hash
    """
    nc = Dataset(f, 'r')
    try:
        pycmor_hash = str(nc.__getattribute__('CCCma_pycmor_hash'))
    except AttributeError as exc:
        pycmor_hash = ''

    return pycmor_hash

def getcreationdate(f):
    """ Returns the file creation date from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of creation date 
    """
    nc = Dataset(f, 'r')
    try:
        creation_date = str(nc.__getattribute__('creation_date'))
    except AttributeError as exc:
        creation_date = ''

    return creation_date

def gettableid(f):
    """ Returns the table_id from a filename and directory path.
        ex. 'Amon', 'Omon', 'fx'
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of table_id
    """
    nc = Dataset(f, 'r')
    return str(nc.__getattribute__('table_id'))


def getexperiment(f):
    nc = Dataset(f, 'r')
    return str(nc.__getattribute__('experiment'))    
   
 
def getrealization(f):
    """ Returns the realization from a filename and directory path.
        This is dependant on the cmip naming convention

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of realization number
    """
    nc = Dataset(f, 'r')
    try:
        realization = str(nc.__getattribute__('realization'))
    except AttributeError as exc:
        realization = str(nc.__getattribute__('realization_index'))
    return realization


def getphysics(f):
    """ Returns the physics index from a filename and directory path.

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of physics index
    """
    nc = Dataset(f, 'r')
    try:
        physics = str(nc.__getattribute__('physics_version'))
    except AttributeError as exc:
        physics = str(nc.__getattribute__('physics_index'))
    return physics


def getforcing(f):
    """ Returns the forcing index from a filename and directory path.

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of forcing index
    """
    nc = Dataset(f, 'r')
    try:
        forcing = str(nc.__getattribute__('forcing'))
    except AttributeError as exc:
        forcing = str(nc.__getattribute__('forcing_index'))
    return forcing


def getinitialization(f):
    """ Returns the initialization index from a filename and directory path.

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of initialization index
    """
    nc = Dataset(f, 'r')
    try:
        initialization = str(nc.__getattribute__('initialization_method'))
    except AttributeError as exc:
        initialization = str(nc.__getattribute__('initialization_index'))
    return initialization


def getrealm(f):
    """ Returns the realm from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of realm
    """
    nc = Dataset(f, 'r')
    try: 
        realm = str(nc.__getattribute__('modeling_realm'))
    except AttributeError as exc:
        realm = str(nc.__getattribute__('realm'))

    if 'seaIce' in realm:
        realm = 'seaIce'
    return realm

def getlongname(f,var):
    """ Returns the long name from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of long name
    """
    nc = Dataset(f, 'r')
    long_name = nc.variables[var].getncattr('long_name')

    return long_name


def getunits(f,var):
    """ Returns the units of variable var from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    string of units
    """
    nc = Dataset(f, 'r')
    units = nc.variables[var].getncattr('units')

    return units


def getmissingvalue(f,var):
    """ Returns the missing value from a filename and directory path.
        This is dependant on a specific directory organization

    Parameters
    ----------
    string : name of file including path

    Returns
    -------
    numpy float32 of missing value
    """
    nc = Dataset(f, 'r')
    missing_value = nc.variables[var].getncattr('missing_value')

    return missing_value



def getrealmcat(realm):
    """ Returns the category of the realm

    Parameters
    ----------
    string : realm

    Returns
    -------
    string of realm category
    """
    if realm == 'aerosol' or realm == 'atmos' or realm == 'seaIce':
        realm_cat = 'atmos'
    elif realm == 'land' or realm == 'landIce':
        realm_cat = 'land'
    else:
        realm_cat = 'ocean'
    return realm_cat

def setcommonlevels(p):
    """ if 'use_common_levels' is set to True, then check that the variable
        is on pressure levels. If not, set 'use_common_levels' to False.
    """
    nc = Dataset(p['ifile'], 'r')
    if not ('plev' in list(nc.variables.keys())):
        p['use_common_levels'] = False
    elif nc.variables['plev'].getncattr('long_name') != 'pressure':
        p['use_common_levels'] = False
    else:
        p['vertical_dim_units'] = nc.variables['plev'].getncattr('units')

def getfiles(plots, directroot, root, run, experiment):
    """ For every plot in the dictionary of plots
        maps the key 'ifile' to the name of the file
        needed to make the plot

    Parameters
    ----------
    plots : list of dictionaries
    run : string
          name of model run

    """
    start = time.time()
    _mkdir()
    _logfile(run, experiment)

    # 2020 cleanup NCS: direct(data)root and (data)root are completely redundant
    # 2020 cleanup NCS: One can just specify dataroot, and traverse below this.
    if directroot:
        files = traverse(directroot)
    else:
        files = traverse(root + '/' + experiment + '-' + run)
    _load_masks(files)
    end = time.time()
    print('Traverse time:', end - start)
    
    realms = {}
    #for f in files:
    #    realms[getvariable(f)] = getrealm(f)
    vf = {} 
    fvr = []

    start = time.time()
    #build filedict with all files in dir
    # 2020 cleanup NCS: These are VERY expensive operations, and are applied without thought below.
    # They should be used minimally, preferably only once.
    def get_filekey(f):
        if run =='CanESM5':
            return (gettableid(f), getvariable(f), getrealization(f))
        else:
            return (getfrequency(f), getvariable(f), getrealization(f))


    # Limit file opening to only files for which the var is requested!
    
    var_list = [p['variable'] for p in plots]     
    var_list = list(set(var_list)) # unique list
    
    for f in files:               
        file_var = getvariable(f)
        if getvariable(f) in var_list:
            file_key = get_filekey(f)
            if file_key in vf:
                vf[file_key].append(f)
            else:
                vf[file_key] = [f]
            realms[getvariable(f)] = getrealm(f)

    end = time.time()
    print('filedict attributes time:', end - start)

        
    #build list of plots requested
    for p in plots:
        if run =='CanESM5':
            fvr.append((p['table_id'], p['variable'], str(p['realization'])))
            for evar in p['extra_variables']:
                fvr.append((p['table_id'], evar, str(p['realization'])))
        else:
            fvr.append((p['frequency'], p['variable'], str(p['realization'])))
            for evar in p['extra_variables']:
                fvr.append((p['frequency'], evar, str(p['realization'])))

    #remove any files from the filedict that are not needed for requested plots
    for key in list(vf.keys()):
        if key not in fvr:
            del vf[key]
    
    startdates = min_start_dates(plots)
    enddates = max_end_dates(plots)
    filedict = _remove_files_out_of_date_range(vf, startdates, enddates)
    filedict = _cat_file_slices(filedict)

    for p in plots:
        if 'ifile' not in p:
            if run =='CanESM5':
                plot_key = (p['table_id'], p['variable'], str(p['realization']))
            else:
                plot_key = (p['frequency'], p['variable'], str(p['realization']))
            
            try:
                p['ifile'] = filedict[plot_key]
            except Exception as e:
                err_str = ("An error occurred when searching for target run" +
                            " {}, {}, {} files\n".format(p['frequency'],p['variable'],p['realization'],p['table_id']) + 
                            "Skipping plots that require these files...\n" +
                            "Error Output: \n {} \n".format(traceback.format_exc()))
                with open('logs/log.txt', 'a') as outfile:
                    outfile.write(err_str)
                print(err_str)
        if 'ifile' not in p:
            continue
        p['realm'] = getrealm(p['ifile'])
        p['long_name'] = getlongname(p['ifile'],p['variable'])
#        p['units'] = getunits(p['ifile'],p['variable'])                               # 2020 cleanup NCS: why are units being hard overwritten? Not right! See #42. 
        p['missing_value'] = getmissingvalue(p['ifile'],p['variable'])
        # 2020 cleanup NCS: much of the information below like `pycmor_label` etc etc is for a very specific application (verification), and likely should not
        # be included in general validate. It is unecessary complexity.
        if p['model_ID'] == 'CanESM5':                                                # 2020 cleanup NCS: there should be no hard-coded checks like this.  
            p['pycmor_hash'] = getpycmorhash(p['ifile'])
            p['creation_date'] = getcreationdate(p['ifile'])
            p['pycmor_label'] = 'converted: ' + p['creation_date'][0:10] + ' (pycmor hash: '  + p['pycmor_hash'][0:8] + ')'
        else:
            p['pycmor_label'] = ''
        p['realm_cat'] = getrealmcat(p['realm'])
        if p['use_common_levels']:
            setcommonlevels(p)
        p['extra_ifiles'] = {}
        p['extra_realms'] = {}
        p['extra_realm_cats'] = {}
        for evar in p['extra_variables']:
            if p['model_ID'] == 'CanESM5':
                p['extra_ifiles'][evar] = filedict[(p['table_id'], evar, str(p['realization']))]
            else:
                p['extra_ifiles'][evar] = filedict[(p['frequency'], evar, str(p['realization']))]
            p['extra_realms'][evar] = realms[evar]                                   # 2020 cleanup NCS: is this needed? Unclear to me 
            p['extra_realm_cats'][evar] = getrealmcat(p['extra_realms'][evar])
        
        if 'fill_continents' not in p['plot_args']:
            if p['realm_cat'] == 'ocean':
                p['plot_args']['fill_continents'] = True

                
    for p in plots[:]:
        if 'ifile' not in p:
            plots.remove(p)


def getidfiles(plots, root, experiment):
    """ Get the files for run IDs for comparison.

    Parameters
    ----------
    plots : list of dictionaries
    experiment : string
                 name of experiment

    """
    ids = []
    for p in plots:
        if p['comp_ids']:
            ids.extend(p['comp_ids'])
        p['id_file'] = {}
    ids = list(set(ids))
    startdates = min_start_dates(plots)
    enddates = max_end_dates(plots)
    for i in ids:
        files = traverse(root + experiment + '-' + i)
        vf = {}
        fvr = []
        for f in files:
            vf[(getfrequency(f), getvariable(f), getrealization(f))] = []
        for f in files:
            vf[(getfrequency(f), getvariable(f), getrealization(f))].append(f)
        for p in plots:
            fvr.append((p['frequency'], p['variable'], str(p['realization'])))
        for key in list(vf.keys()):
            if key not in fvr:
                del vf[key]
        filedict = _remove_files_out_of_date_range(vf, startdates, enddates)
        filedict = _cat_file_slices(filedict)
        for p in plots:
            if i in p['comp_ids']:
                p['id_file'][i] = filedict[(p['frequency'], p['variable'], str(p['realization']))]


def getensfiles(plots, directroot, run):
    """ Get the files for realizations for comparison.

    Parameters
    ----------
    plots : list of dictionaries

    """
    startdates = min_start_dates(plots)
    enddates = max_end_dates(plots)
    files = traverse(directroot)
    for p in plots:
        p['ensemble_files'] = {}
        if p['comp_ensembles']:
            vf = defaultdict(list)
            for f in files:
                if getvariable(f) != p['variable'] or getfrequency(f) != p['frequency'] or getrealm(f) != p['realm']:
                    continue
                vf[(getrealization(f), getinitialization(f), getphysics(f), getforcing(f))].append(f)
            filedict = _remove_files_out_of_date_range(vf, startdates, enddates)
            filedict = _cat_file_slices(filedict)
            
            for enskey in list(filedict.keys()):
                if (enskey[0] not in p['comp_ensembles']['realizations'] or 
                    enskey[1] not in p['comp_ensembles']['initializations'] or 
                    enskey[2] not in p['comp_ensembles']['physics'] or 
                    enskey[3] not in p['comp_ensembles']['forcings']):
                        del filedict[enskey]
            
            for enskey, ensfile in filedict.items():    
                enslabel = 'r' + enskey[0] + 'i' + enskey[1] + 'p' + enskey[2] + 'f' +  enskey[3]
                p['ensemble_files'][enslabel] = ensfile


def remfiles(del_mask=True, del_ncstore=True, del_netcdf=True, del_cmipfiles=True, **kwargs):
    """ Option to delete the directories used to store processed .nc files

    Paremeters
    ----------
    del_fldmeanfiles : boolean
    del_mask : boolean
    del_ncstore : boolean
    del_remapfiles : boolean
    del_trendfiles : boolean
    del_zonalfiles : boolean
    """
    if del_mask:
        os.system('rm -rf mask')
    if del_ncstore:
        os.system('rm -rf ncstore')
    if del_netcdf:
        os.system('rm -rf netcdf')
    if del_cmipfiles:
        os.system('rm -rf cmipfiles')

def link_files_to_viewer(output_root, experiment, run):
    """
    link all files found in the plots directory to the users public_html folder,
    following the path ~/*user*/public_html/validate_plots/*run*-*time*
    if only a plots.tar.gz file is present, extract it and then link those files
    """

    print("linking files to public_html...")
    time = datetime.datetime.now().strftime("%d%m%Y-%H%M%S")
    username = getpass.getuser()

    #build the path where files will be linked to in the users public HTML directory
    publicHtmlPath = os.path.join('/home/' ,username ,'public_html/')
    validateHtmlPath = os.path.join(publicHtmlPath, 'validate_plots/')
    runHtmlPath = os.path.join(validateHtmlPath, f'{run}-{time}')
    
    plotsPath = os.path.join(output_root, "plots")

    #if plots folder is not present, try and extract tar file instead
    if not os.path.exists(plotsPath):
        extract_tarfile(os.path.join(output_root, "plots.tar.gz"), plotsPath)
        
    plotFiles = traverse(plotsPath)

    try:
        os.mkdir(runHtmlPath)
    except FileNotFoundError: #one or more of the parent directories in our runHtmlPath is missing
        if not os.path.isdir(publicHtmlPath): #if the user doesn't have a public_html folder
            print("public_html directory not found, creating...")
            os.mkdir(publicHtmlPath) #create it
        print("pulic_html/validate_plots directory not found, creating...")
        os.mkdir(validateHtmlPath) #create the parent validate folder within public_html
        os.mkdir(runHtmlPath) #now try and create runHtmlPath again
       
    
    for file in plotFiles: #for each file in plots directory, link it to our newly build folder
        fileName = os.path.basename(file) #extract filename from full path
        fullPath = os.path.join(runHtmlPath, fileName)
        os.symlink(file, fullPath)
    
    print((f"\n\nall plot files have been linked to your public_html folder and\n"
           f"are viewable here: https://goc-dx.science.gc.ca/~{username}/validate_plots/{run}-{time}\n"
           f"if you wish to set up a plot viewer instance, you may do so here: https://goc-dx.science.gc.ca/%7Escrd104/Web-Viewers/url_builder/\n"
           f"fill in {username} under user account and validate_plots/{run}-{time} under directory\n"))


def getobsfiles(plots, obsroot):
    obsdirectories = [o for o in os.listdir(obsroot) if os.path.isdir(os.path.join(obsroot,o))]
    for o in obsdirectories:
        obspath = os.path.join(obsroot, o)
        getobs (plots, obspath, o)


def getobs(plots, obsroot, o):
    """ For every plot in the dictionary of plots
        if an observations file is needed it
        maps the key 'comp_file' to a file containg observations.
        If no observations file is found it changes the comparison
        plots to false and prints out a warning.

    Parameters
    ----------
    plots : list of dictionaries
    obsroot : string
              directory path to find observations
    """
    obsfiles = traverse(obsroot)
    variables = _variable_dictionary(plots)
    for f in obsfiles:
        var = getvariable(f)
        if var in variables:
            variables[var].append(f)
    for p in plots:
        if o in p['comp_obs']:
            if 'obs_file' not in p:
                p['obs_file'] = {}
            try:
                p['obs_file'][o] = variables[p['variable']][0]
            except Exception as e:
                err_str = ("An error occurred while looking for {} ".format(p['variable']) + 
                            "observation data in the {} dataset \n".format(o) +
                            "Removing {} comparison for this plot\n".format(o) +
                            "Error Output: \n {} \n".format(traceback.format_exc()))
                with open('logs/log.txt', 'a') as outfile:
                    outfile.write(err_str)
                print(err_str)
                p['comp_obs'].remove(o)
        if o in p['extra_obs']:
            if 'extra_obs_files' not in p:
                p['extra_obs_files'] = {}
            for i, name in enumerate(p['extra_obs'][:]):
               if name == o:
                    try:
                        p['extra_obs_files'][p['extra_variables'][i]] = variables[p['extra_variables'][i]][0]
                    except:
                        err_str = ("An error occurred while looking {} ".format(p['extra_variables'][i]) +
                                    "observation data in the {} data \n".format(o) + 
                                    "Removing {} comparison for this plot \n".format(o) +
                                    "Error Output: \n {} \n".format(traceback.format_exc()))
                        with open('logs/log.txt', 'a') as outfile:
                            outfile.write(err_str)
                        print(err_str)
                        p['extra_variables'].pop(i)
                        p['extra_scales'].pop(i)
                        p['extra_comp_scales'].pop(i)
                        p['extra_shifts'].pop(i)
                        p['extra_comp_shifts'].pop(i)                       
                        p['extra_obs'].pop(i)
                                          
def model_files(var, model, table_id, realm, expname, frequency, cmipdir, proc_cmipdir="cmipfiles/"):
    """ 
        Creates an ensemble object from the files located in cmipdir for the 
        desired variable, model, experiment, and output freq.

        For all realizations found in cmipdir, this also checks if the
        data is contained in multiple files (time-slices) and searches 
        for a concatenated version of them if so. If it can't find a 
        concatenated version, it uses cdo mergetime to merge them and
        stores the file in './cmipfiles/'.
        
        model_files() searches for the concatenated version in two locations:
            1. local './cmipfiles/'
            2. proc_cmipdir
        Note, if left to the default arg, this will result in the code 
        checking in './cmipfiles/' twice. 
    """
    # prep
    if not proc_cmipdir.endswith('/'): proc_cmipdir += '/'
    os.environ["SKIP_SAME_TIME"] = "1"
    
    # build ensemble. Special case for CanESM5. Otherwise try format for the original cmip data, if no vars returned try format for
    # canESM2 large ensemble
    if model == 'CanESM5':
        version_list =  os.listdir(os.path.join(cmipdir, table_id + '/' + var + '/gn/'))
        if len(version_list) > 1: 
            print('Note: More than one CanESM5 version is present, only the first will be used')
        prefix = os.path.join(cmipdir, table_id + '/' + var + '/gn/' + version_list[0] + '/')
        ensstring = os.path.join(prefix,'{}_*{}_*{}_{}_*.nc'.format(var,table_id,model,expname))
        kwargs = {'separator':'_', 'variable':0, 'realm':1, 'model':2, 'experiment':3, 'realization':4, 'dates':6}
        ens = cd.mkensemble(ensstring, prefix=prefix, kwargs=kwargs)
    else:
        prefix = os.path.join(cmipdir,var+'/')
        ensstring = os.path.join(prefix,'{}_*{}_*{}_{}_*.nc'.format(var,frequency,model,expname))
        ens = cd.mkensemble(ensstring, prefix=prefix)
    
    if ((ens.objects('variable')) == []):
        prefix = os.path.join(cmipdir,frequency + '/' + realm + '/' + var + '/r1i1p1/')
        ensstring = os.path.join(prefix,'{}_*{}_*{}_{}_*.nc'.format(var,frequency,model,expname))
        ens = cd.mkensemble(ensstring, prefix=prefix)
    
    # check if the variable object's data (for each realization) 
    # is contained in one file or not
    for var_obj in ens.objects('variable'):
        child_objs  = var_obj.children 
        files       = [ c.name for c in child_objs ]
        if len(files) > 1: 
            realization = var_obj.getDictionary()['realization']
            print(('time-slices detected for {},{},{},{},{}'.format(model,var,expname,frequency,realization)))
            print('\t searching for concatenated files')
            infiles = ' '.join(files)
            # check if the concatenated version exists in proc_cmipdir or 
            #   locally in 'cmipfiles'. If not, create it locally
            min_startdate   = min([ c.start_date for c in child_objs ])
            max_enddate     = max([ c.end_date for c in child_objs ])
            outfile         = (os.path.split(child_objs[0].getNameWithoutDates())[1] + 
                                '_{}-{}.nc'.format(str(min_startdate),str(max_enddate)))
            local_outfile   = os.path.join('cmipfiles',outfile)
            local_exist     = os.path.isfile(local_outfile)
            stored_outfile  = os.path.join(proc_cmipdir,outfile)
            stored_exist    = os.path.isfile(stored_outfile)
            
            if local_exist:
                print(('{} exists locally in cmipfiles/'.format(outfile)))
                print('\t using local version \n')
                final_outfile = local_outfile
            elif stored_exist:
                print(('{} exists in {}'.format(outfile,proc_cmipdir)))
                print('\t using stored version \n')
                final_outfile = stored_outfile
            else:
                print(('{} not found... creating it locally in cmipfiles/'.format(outfile)))
                # join the files and store new file locally
                final_outfile = local_outfile
                catstring = 'cdo mergetime ' + infiles + ' ' + final_outfile
                os.system(catstring)
                print('')

            f = cd.DataNode('ncfile', final_outfile, parent=var_obj, start_date=min_startdate, end_date=max_enddate)
            var_obj.children = [f]
    
    mfiles = ens.lister('ncfile')
    return mfiles, ens
    
def model_average(ens, var, model, expname, freq, proc_cmipdir=""):
    """ Creates and stores a netCDF file with the average data
        across the realizations for a given variable, model, and experiment
        Returns the name of the created file.

        Before creating the file, first checks if it exists locally in 
        './cmipfiles/', and if it isn't found there, then checks in 
        proc_cmipdir. Finally, if the file isn't found in either of 
        these locations, it creates it and stores it in './cmipfiles/'.
    """
    mean_f          = '{}_{}_{}_{}_realization_mean.nc'.format(var,model,freq,expname)
    stored_mean_f   = os.path.join(proc_cmipdir,mean_f)
    local_mean_f    = os.path.join('cmipfiles',mean_f)

    if not os.path.isfile(local_mean_f):
        if not os.path.isfile(stored_mean_f):
            print(('Calculating {} {} {} realization mean for {} and storing in cmipfiles\n'.format(var,expname,freq,model)))
            means, stdevs = cd.ens_stats(ens, var)
            shutil.move(means[0], local_mean_f)
            os.remove(stdevs[0])
            mean_f = local_mean_f
        else:
            print(('{} {} {} realization mean for {} already exists in \n {} \n'.format(var,expname,freq,model,proc_cmipdir)))
            mean_f = stored_mean_f
    else:
        print(('{} {} {} realization mean for {} already exists locally in cmipfiles\n'.format(var,expname,freq,model)))
        mean_f = local_mean_f
    return mean_f

def cmip_files(model_files):
    files = list(model_files.values())
    allfiles = [item for sublist in files for item in sublist]
    return list(set(allfiles))

def get_cmip_average(plots, directory):
    averagefiles = traverse(directory)

    variables = _variable_dictionary(plots)
    for f in averagefiles:
        var = getvariable(f)
        if var in variables:
            variables[var].append(f)
    for p in plots:
        if p['comp_cmips']:
            for cfile in variables[p['variable']]:
                if getfrequency(cfile) == p['frequency'] and getexperiment(cfile) == p['experiment']:
                    p['cmip5_file'] = cfile
                    break
            else:
                p['cmip5_file'] = None

# 2020 cleanup NCS: The function below does not work. It is not well considered, and needs to be replaced.
# 2020 cleanup NCS: The `comp_models` functionality, leveraging the cmipdata based ensemble means works, and
# 2020 cleanup NCS: should be a starting point. Once all realization means are made, they can be remapped and
# 2020 cleanup NCS: used to calculate the ensemble mean across models.

def cmip_average(var, frequency, files, sd, ed, expname, mean_cmipdir):
    """ Creates and stores a netCDF file with the average data
        across all the models provided.
        Returns the name of the created file.
    """
    averagefiles = traverse(mean_cmipdir)
    
    out = 'ENS-MEAN_cmipfiles/' + var + '_' + 'cmip5.nc'
    # skip if the new file was already made
    if not os.path.isfile(out):
        newfilelist = []
        newerfilelist = []
        for f in files:
            time = f.replace('.nc', '_time.nc')
            # try to select the date range
#            try:
            
            os.system('cdo -L seldate,' + sd + ',' + ed + ' -selvar,' + var + ' ' + f + ' ' + time)
#            cdo.seldate(sd+','+ed, options = '-L', input='-selvar,' + var + ' ' + f, output=time)
#            except:
                # don't append filename to the list if it was not in the date range
#                pass
#            else:
            newfilelist.append(time)
        for f in newfilelist:
            remap = f.replace('.nc', '_remap.nc')
            
            # try to remap the files
            try:
                cdo.remapdis('r360x180', input=f, output=remap)
            except:
                # don't append the filename if it could not be remapped
                pass
            else:
                newerfilelist.append(remap)

        filestring = ' '.join(newerfilelist)
        
        # compute the mean across the models
        cdo.ensmean(input=filestring, output=out)
    return out

# 2020 cleanup NCS: could be cleaned up, and replaced with an intake-esm database of available models.
def getcmipfiles(plots, expname, cmipdir, proc_cmipdir, mean_cmipdir):
    """ Loop through the plots and create the comparison files if cdo operations are needed 
        and map the keys in the compare dictionary to the correct file names.

    Inputs
    ------
    plots : list
      Plot dictionaries in a list
    expname: str

    cmipdir: str
      Top-level directory of cmipdata to traverse

    proc_cmipdir: str
          

    """
    # get the date ranges need for each variable
    startdates = min_start_dates(plots)
    enddates = max_end_dates(plots)
    cmip5_variables = {}  
    for p in plots:
        p['model_files'] = {}
        p['model_file'] = {}
        p['cmip5_files'] = []
        p['cmip5_file'] = None
        if p['comp_models'] or p['comp_cmips']:
            # map the file names of the comparison files to the model names
            for model in p['comp_models'][:]:
                try:
                    p['model_files'][model], ens = model_files(p['variable'], model, p['table_id'], p['realm'], expname, p['frequency'], 
                                                                cmipdir, proc_cmipdir)
                    p['model_file'][model] = model_average(ens, p['variable'], model, expname, p['frequency'], proc_cmipdir)
                except Exception as e:
                    err_str = ("An error occurred while looking for/processing {} ".format(p['variable']) +
                                "files for {}\n".format(model) + 
                                "Removing {} from the comparisons for this plot".format(model) + '\n' +
                                "Error Output: \n {} \n".format(traceback.format_exc())) 
                    with open('logs/log.txt', 'a') as outfile:
                        outfile.write(err_str)
                    print(err_str)
                    p['comp_models'].remove(model)
                    try:
                         p['comp_cmips'].remove(model)
                    except: 
                        pass
                    
            for model in p['comp_cmips'][:]:
                if model not in p['comp_models']:
                    try:
                        p['model_files'][model], ens = model_files(p['variable'], model, p['table_id'], p['realm'], expname, p['frequency'], 
                                                                    cmipdir, proc_cmipdir)
                    except Exception as e:
                        err_str = ("An error occurred while looking for/processing {} ".format(p['variable']) +
                                    "files for {}\n".format(model) + 
                                    "Removing {} from the comparisons for this plot".format(model) + 
                                    "Error Output: \n {} \n".format(traceback.format_exc())) 
                        with open('logs/log.txt', 'a') as outfile:
                            outfile.write(err_str)
                        print(err_str)
                        # remove the model from the list if no comparison files were found
                        p['comp_cmips'].remove(model)

    get_cmip_average(plots, mean_cmipdir)
                        
    for p in plots:
        if p['comp_cmips']:
            # map the file name of the comparison file to cmip5 in compare dictionary
            try:
                files = {}
                for f in p['comp_cmips']:
                    files[f] = p['model_files'][f]                         
                p['cmip5_files'] = cmip_files(files)
# 2020 cleanup NCS: This is commented because it always leads into the except block when uncommented, which breaks plotting down the road. Tested with various scenarios.
#                p['cmip5_file'] = cmip_average(p['variable'], p['frequency'], p['cmip5_files'], str(startdates[p['variable']]) + '-01', str(enddates[p['variable']]) + '-01', expname)
            except:
                p['comp_cmips'] = []
                print('EXCEPT in comp_cmips')


def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))

def extract_tarfile(input_filename, output_dir):
    """
    given a tarfile specified by input_filename, extract all contents to a
    directory specified by output_dir
    """
    try:
        with tarfile.open(input_filename, "r:gz") as tar:
            tar.extractall(path=output_dir)
    except FileNotFoundError:
        print("tarfile could not be found, extraction aborted...")
        
def move_tarfile(location):
    if location is not None:
        make_tarfile('plots.tar.gz', 'plots')
        make_tarfile('logs.tar.gz', 'logs')
        plots_new_name = os.path.join(location, 'plots.tar.gz')
        logs_new_name = os.path.join(location, 'logs.tar.gz')
        os.system('scp plots.tar.gz "%s"' % plots_new_name)
        os.system('scp logs.tar.gz "%s"' % logs_new_name)        
            

if __name__ == "__main__":
    pass
