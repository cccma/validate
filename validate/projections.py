"""
projections
===============
This module contains the functions that will produce the plots
using matplotlib.

.. moduleauthor:: David Fallis
"""

import matplotlib as mpl
import subprocess, os, glob, brewer2mpl, cdo
import numpy as np
from numpy import mean, sqrt, square
import cartopy.crs as ccrs
import cartopy.util as cutil
import matplotlib.pyplot as plt
from .discrete_cmap import discrete_cmap
from netCDF4 import Dataset
from .colormaps import viridis, magma, inferno, plasma
from .taylor import TaylorDiagram
from operator import itemgetter
from math import floor, ceil
import matplotlib.patches as mpatches
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
cdo = cdo.Cdo()
plt.close('all')
font = {'size': 9}
plt.rc('font', **font)


def default_pcolor_args(data, anom=False, missing_value=np.nan):
    """Returns a dict with default pcolor params as key:value pairs

    Parameters
    ----------
    data : numpy array
    anom : boolean
           True if positive/negative display is wanted

    Returns
    -------
    dictionary
    """
    # Set 3-std range for colorbar to exclude outliers.
    data = np.array(data, dtype=np.float64)
    data_masked = np.ma.masked_values(data, missing_value)
    if anom:
        # For anomalies, center range around 0
        anom_max = abs(data_masked).mean() + abs(data_masked).std() * 3.0
        vmin = -1 * anom_max
        vmax = anom_max
        # Anomaly cmap
        cmap = anom_cmap()

    else:
        mean = data_masked.mean()
        std = data_masked.std()
        dmax = data_masked.max()
        dmin = data_masked.min()       
        # otherwise, center around the mean
        vmin = mean - std * 3.0
        vmax = mean + std * 3.0
        if vmax > dmax and vmin < dmin:
            vmax = dmax
            vmin = dmin
        elif vmin < dmin:
            vmax = vmax + dmin - vmin
            if vmax > dmax:
                vmax = dmax
            vmin = dmin
        elif vmax > dmax:
            vmin = vmin + dmax - vmax
            if vmin < dmin:
                vmin = dmin
            vmax = dmax            
        # New mpl, colorblind friendly, continuously varying, default cmap
        cmap = inferno

    d = {'vmin': vmin,
         'vmax': vmax,
         'cmap': cmap,
         'rasterized': True
         }

    return d

def anom_cmap():
    """return a discrete blue-red cmap from colorbrewer"""
    ncols = 11
    cmap_anom = brewer2mpl.get_map('RdBu', 'diverging', ncols,
                                   reverse=True).mpl_colormap
    cmap_anom = discrete_cmap(ncols, cmap_anom)
    return cmap_anom

def draw_stipple(pvalues, lon, lat, ax, alpha):
        slons = []
        slats = []
        for index, value in np.ndenumerate(pvalues):
            if index[1]%4 == 0 and index[0]%2 == 0:
                if value < alpha:    
                    slons.append(lon[index[1]])
                    slats.append(lat[index[0]])
        #a,b = m(slons, slats)
        ax.plot(slons,slats, '.', markersize=0.3, color='k', zorder=1)

def draw_trend_stipple(data, cvalues, lon, lat, ax):        
        slons = []
        slats = []
        for index, value in np.ndenumerate(cvalues):
            if index[1]%4 == 0 and index[0]%2 == 0:
                if abs(value) < data[index[0]][index[1]]:             
                    slons.append(lon[index[1]])
                    slats.append(lat[index[0]])
        ax.plot(slons,slats, '.', markersize=0.3, color='k', zorder=1)      

def create_discrete_cmap(ax, num_bins, lons, lats, data, base_cmap, bounds=None):
    '''
        create a discrete cmap based on the color scheme passed in pcolor_args
        inspired by https://gist.github.com/jakevdp/91077b0cae40f8f8244a
    '''
    color_list = base_cmap(np.linspace(0,1,num_bins))
    cmap_name = base_cmap.name + str(num_bins)
    discrete_cmap = mpl.colors.LinearSegmentedColormap.from_list(cmap_name, color_list,num_bins)
    norm = get_norm(bounds, discrete_cmap)
    return discrete_cmap, norm

def get_step(diff, num_bins):
    '''
        given the difference between two values, and the number of even bins desired between them. Return the
        step between each bin, in other words the size of each bin. 
    '''
    step = diff/num_bins
    if step > 1:
        step = ceil(step)
    return step

def get_bins(vmin, vmax, total_bins):
    '''
        given the max, min, and number of bins desired in a discrete colorbar,
        return the bounds: where there should be color transitions
        and the ticks: where each color will be labeled, at the center of each color.
    '''

    start = floor(vmin)
    stop = ceil(vmax)

    #ensure that colorbar is centered on 0 for +/- plots
    if abs(vmin) == abs(vmax):
        diff = vmax
        num_bins = (total_bins - 1)/2
        step = get_step(diff, num_bins)
        negative_half = np.arange(0,start-(2*step), -step)
        negative_half = negative_half[::-1][:-1]
        positive_half = np.arange(0, stop+(2*step), step)
        ticks = np.concatenate([negative_half, positive_half])
    #otherwise just create an even distribution 
    else:
        diff = stop - start
        step = get_step(diff, total_bins)
        ticks = np.arange(start-step,stop+step,step)

    bounds = [ticks[0]-(step/2)]
    for i in range(len(ticks)):
        bounds.append(bounds[i] + step)
    return bounds, ticks

def get_norm(bounds, cmap):
    '''
        generate a colormap index based on discrete intervals (bounds)
    '''
    return mpl.colors.BoundaryNorm(bounds, cmap.N)

def add_cbar(position, fig, ax, co, size='5%', pad=0.075, bounds=None, norm=None, ticks=None, **kwargs):
    '''
        taken from gmd notebook (https://gitlab.com/cccma/gmd-figs) plotting_module.py. Adds a colorbar to an axes.   
    '''
    divider = make_axes_locatable(ax)
    ax_cb = divider.append_axes(position, size=size, pad=pad, axes_class=plt.Axes)

    if position in ['top', 'bottom']:
        orientation='horizontal'
    if position in  ['right','left']:
        orientation='vertical'
    if bounds is not None:                                                    
        plt.colorbar(co, cax=ax_cb, orientation=orientation, ticks=ticks, boundaries=bounds, norm=norm, **kwargs)
    else:
        plt.colorbar(co, cax=ax_cb, orientation=orientation, **kwargs)

    if position == 'left':
        ax_cb.yaxis.set_ticks_position('left')
        ax_cb.yaxis.set_label_position('left')

def worldmap(projection, lon, lat, data, plot_Number=None, pvalues=None, cvalues=None, alpha=None, ax=None,
             ax_args=None, pcolor_args=None, cblabel='', anom=False, rmse=False,
             latmin=-80, latmax=80, lonmin=0, lonmax=360, lon_0=-180, draw_contour=False, 
             label=None, fill_continents=False, draw_parallels=False, draw_meridians=False, 
             plot={}, discrete_colorbar=True):
    '''
        creates a single plot on the overarching figure using Cartopy, projections can be 
        Robinson, mercator or polar. Uses parameters derived from the plot dictionary to configure the plot.
    '''
    #if pcolor_args does not exist fill it with default values
    if not pcolor_args:
        pcolor_args = default_pcolor_args(data, anom, missing_value=plot['missing_value'])
    for key, value in default_pcolor_args(data, anom).items():
        if key not in pcolor_args or (pcolor_args[key] is None):
            pcolor_args[key] = value
    #check if there will be one or mulitple plots on the same figure
    if plot_Number:
        height = 3
        width = 1
        index = plot_Number
    else:
        height = 1
        width = 1
        index = 1


    maps = []
    m2 = None
    axes = []
    ax2 = None
    proj = None
    proj2 = None

    fig = plt.gcf()

    if projection == 'global_map':
        proj = ccrs.Robinson(central_longitude=lon_0)
        ax1 = plt.subplot(height,width, index, projection=proj, **ax_args)
        parallel_labels = [1, 0, 0, 0]
        meridian_labels = [0, 0, 0, 0]
    elif projection == 'mercator':
        proj = ccrs.Mercator(central_longitude=lon_0, min_latitude=latmin, max_latitude=latmax, latitude_true_scale=20)
        ax1 = plt.subplot(height,width, index, projection=proj, **ax_args)
        parallel_labels = [0, 0, 0, 0]
        meridian_labels = [0, 0, 0, 0]
    elif projection == 'polar_map':
        proj = ccrs.NorthPolarStereo()
        ax1 = plt.subplot(height,width,index, projection=proj, **ax_args)
        ax1.set_extent([-180, 180, 90, 60], ccrs.PlateCarree())
        data, lon = cutil.add_cyclic_point(data, coord=lon)
        parallel_labels = [0, 0, 0, 0]
        meridian_labels = [0, 0, 0, 0]
    elif projection == 'polar_map_south':
        proj = ccrs.SouthPolarStereo()
        ax1 = plt.subplot(height,width,index, projection=proj, **ax_args)
        ax1.set_extent([-180, 180, -90, -60], ccrs.PlateCarree())
        data, lon = cutil.add_cyclic_point(data, coord=lon)
        parallel_labels = [0, 0, 0, 0]
        meridian_labels = [0, 0, 0, 0]
    elif projection == 'polar_map_both':
        if plot_Number in range (1,3): #if first or second plot
            fig.suptitle(ax_args['title']) #set overall figure title
            ax_args['title'] = None #blank title for first two plots 

        proj = ccrs.NorthPolarStereo()
        ax1 = plt.subplot(height,width+1,index, projection=proj, **ax_args)
        ax1.set_extent([-180, 180, 90, 60], ccrs.PlateCarree())
        proj2 = ccrs.SouthPolarStereo()
        ax2 = plt.subplot(height,width+1,index+1, projection=proj2, **ax_args)
        ax2.set_extent([-180, 180, -90, -60], ccrs.PlateCarree())
        parallel_labels = [0, 0, 0, 0]
        meridian_labels = [0, 0, 0, 0]
        data, lon = cutil.add_cyclic_point(data, coord=lon)

    maps.append(proj)
    maps += [proj2] if proj2 is not None else []
    axes.append(ax1)
    axes += [ax2] if ax2 is not None else []
    base_cmap = pcolor_args['cmap']
    pcolor_args.pop('cmap', None) #remove old cmap from pcolor_args so its not doubled up
    #loop through all axes generated and plot colormap, add colorbar, and draw any extra information
    #note: more than one axes only generated in polor_map_both case
    for proj, ax in zip(maps, axes):
        lons, lats = np.meshgrid(lon, lat) 
        #create colormesh
        if discrete_colorbar:
            if anom:
                num_bins = 11
            else:
               num_bins = 15
            bounds, ticks = get_bins(pcolor_args['vmin'], pcolor_args['vmax'], num_bins)
            discrete_cmap, norm = create_discrete_cmap(ax, num_bins, lons, lats, data, base_cmap, bounds=bounds)
            colorMesh = ax.pcolormesh(lons, lats, data, transform = ccrs.PlateCarree(), cmap=discrete_cmap, norm=norm, **pcolor_args)
            #add colorbar
            add_cbar('right', fig, ax, colorMesh, extend='both', label=cblabel, bounds=bounds, norm=norm, ticks=ticks) 
        else:
            colorMesh = ax.pcolormesh(lons, lats, data, transform = ccrs.PlateCarree(), **pcolor_args)
            #add colorbar
            add_cbar('right', fig, ax, colorMesh, extend='both', label=cblabel)
        ax.coastlines()
        #draw contour lines
        if draw_contour:
            ax.contour(lons, lats, data, 10, colors='k', transforms=ccrs.PlateCarree(), linewidths=1)
        #draw parallels
        if draw_parallels:
            ax.drawparallels(np.arange(-80, 81, 20), labels=parallel_labels, ax=ax, fontsize=9)
        #draw meridians
        if draw_meridians:
            ax.drawmeridians(np.arange(0, 360, 90), labels=meridian_labels, yoffset=0.5e6, ax=ax, fontsize=9)
        #draw pvalue stipple
        if pvalues is not None:
            draw_stipple(pvalues, lon, lat, ax, alpha)
        #draw cvalue stipple
        if cvalues is not None:
            draw_trend_stipple(data, cvalues, lon, lat, ax)
        #set label
        if label is not None:
            ax.text(0,-0.01, label, fontsize=7,verticalalignment='top', transform=ax.transAxes)

def section(x, z, data, ax=None, rmse=False, pvalues=None, alpha=None, ax_args=None, pcolor_args=None, plot={}, cblabel='', anom=False, cbaxis=None, discrete_colorbar=True):
    """Pcolor a var in a section, using ax if supplied"""
    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
        fig.subplots_adjust(top=0.8, right=0.8)
    else:
        fig = plt.gcf()

    if not pcolor_args:
        pcolor_args = default_pcolor_args(data, anom, missing_value=plot['missing_value'])
    for key, value in default_pcolor_args(data,anom).items():
        if key not in pcolor_args or (pcolor_args[key] is None):
            pcolor_args[key] = value

    if discrete_colorbar:
        if anom:
            num_bins = 13
        else:
            num_bins = 15
        bounds, ticks = get_bins(pcolor_args['vmin'], pcolor_args['vmax'], num_bins)
        discrete_cmap, norm = create_discrete_cmap(ax, num_bins, x, z, data, pcolor_args['cmap'], bounds=bounds)
        pcolor_args.pop('cmap', None) #remove old cmap from pcolor_args so its not doubled up
        colorMesh = ax.pcolormesh(x, z, data, cmap=discrete_cmap, norm=norm, **pcolor_args)
        add_cbar('right', fig, ax, colorMesh, extend='both', label=cblabel, bounds=bounds, norm=norm, ticks=ticks)
    else:
        colorMesh = ax.pcolormesh(x, z, data,shading='gouraud', **pcolor_args)
        add_cbar('right', fig, ax, colorMesh, extend='both', label=cblabel)



    if plot['variable'] == 'msftmyz':
        cts = np.around(np.arange(-25,25, 2), decimals=1)
        cs = ax.contour(x, z, data, cts, colors=['k'], vmin=pcolor_args['vmin'],
                   vmax=pcolor_args['vmax'])
        plt.clabel(cs,  inline=True, fmt='%r', fontsize= 3)
    else:
        ax.contour(x, z, data, colors=['k'], vmin=pcolor_args['vmin'],
                   vmax=pcolor_args['vmax'])
               

    ax.invert_yaxis()
    ax.autoscale(True, axis='both', tight='both')

    ax.set_yscale(plot['set_yscale'])

    if ax_args:
        plt.setp(ax, **ax_args)

    if pvalues is not None:
        slons = []
        sdepths = []
        for index, value in np.ndenumerate(pvalues):
            if index[1]%4 == 0:
                if value < alpha:
                    slons.append(x[index[1]])
                    sdepths.append(z[index[0]])
        ax.plot(slons, sdepths, '.', markersize=0.2, color='k')

    box = ax.get_position()
    vals = ["{:.2E}".format(np.nanmin(data)), "{:.2E}".format(np.nanmax(data))]
    plot['stats'] = {'min': float(vals[0]),
                     'max': float(vals[1]),
                     }
    snam = ['min: ', 'max: ']    
    val = [s + v for s, v in zip(snam, vals)]
    ax.text(0, -0.20, '  '.join(val), horizontalalignment='left', verticalalignment='bottom', fontsize=7, transform=ax.transAxes) 

def timeseries(x, data, ax=None, ax_args=None, label='model', plot={}, color=None, zorder=None):
    """ Makes a timeseries line plot, using ax if supplied
    """
    if data.shape != x.shape:
        data = data[:x.shape[0]]
    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    else:
        fig = plt.gcf()

    ax.plot(x, data, label=label, color=color, zorder=zorder)
    ax.set_xlim(xmin=np.min(x),xmax=np.max(x))
    plt.setp(ax, **ax_args)
    plot['stats'] = 'N/A'


def zonalmean(x, data, ax=None, ax_args={}, label='model', plot={}, color=None, zorder=None):
    """ Makes a zonal mean line plot, using ax if supplied
    """
    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    else:
        fig = plt.gcf()
    ax.plot(x, data, label=label, color=color, zorder=zorder)
    plt.xlim(-90, 90)
    if ax_args:
        plt.setp(ax, **ax_args)
    plot['stats'] = 'N/A'


def taylordiagram(refdata, labelled_data, unlabelled_data, fig=None, ax_args=None, plot={}):

    flatrefdata = refdata.flatten()
    refstd = refdata.std(ddof=1)
    for i, (d, n, c) in enumerate(labelled_data):
        labelled_data[i] = d.flatten(), n, c

    plot['stats'] = {'obserations': {'standard deviation': float(refstd)}}
    
    for i, (d, n) in enumerate(unlabelled_data):
        unlabelled_data[i] = d.flatten(), n

    labelled_samples = [[m.std(ddof=1), np.ma.corrcoef(flatrefdata, m)[0, 1], n, c] for m, n, c in labelled_data]
    unlabelled_samples = [[m.std(ddof=1), np.ma.corrcoef(flatrefdata, m)[0,1], n] for m, n in unlabelled_data]
    
    if not fig:
        fig = plt.figure()
    else:
        fig = plt.gcf()

    stdrange = max(labelled_samples, key=itemgetter(1))[0] * 1.3 / refstd
    if stdrange <= refstd * 1.5 / refstd:
        stdrange = refstd * 1.5 / refstd

    dia = TaylorDiagram(refstd, fig=fig, label=plot['comp_obs'][0], srange=(0, stdrange))
#    colors = plt.matplotlib.cm.jet(np.linspace(0, 1, len(samples)))
    
    for i, (stddev, corrcoef, n, c) in enumerate(labelled_samples):
        if corrcoef < 0:
            corrcoef = 0
        plot['stats'][n] = {'standard deviation': float(stddev),
                            'correlation coefficient': float(corrcoef)} 
        dia.add_sample(stddev, corrcoef,
                       marker='.', ms=12, ls='',
                       mfc=c, mec=c,
                       label=n, zorder=2)
    fig.legend(dia.samplePoints,
               [p.get_label() for p in dia.samplePoints],
               numpoints=1, prop=dict(size='small'), loc='upper right')
    
    for i, (stddev, corrcoef, n) in enumerate(unlabelled_samples):
        if corrcoef <= 0:
            continue
        plot['stats'][n] = {'standard deviation': float(stddev),
                            'correlation coefficient': float(corrcoef)}         
        dia.add_sample(stddev, corrcoef,
                       marker='.', ms=5, ls='',
                       mfc='grey', mec='grey',
                       label=None, zorder=1) 
  
    dia.add_grid()

    contours = dia.add_contours(colors='0.5')
    plt.clabel(contours, inline=1, fontsize=10)

    if 'title' in ax_args:
        plt.title(ax_args['title'])

def taylor_from_stats(labelled_data, unlabelled_data, obs_label='observation', fig=None, label=None, ax_args=None):
    if not fig:
        fig = plt.figure()
    else:
        fig = plt.gcf()

    stdrange = max([item['std'] for item in labelled_data]) * 1.3
    if stdrange <= 1.5:
        stdrange = 1.5
 
    dia = TaylorDiagram(1, fig=fig, label=obs_label, srange=(0, stdrange))
    handles_dictionary = {}
    for i, sample in enumerate(labelled_data):
        if sample['corrcoef'] < 0:
            sample['corrcoef'] = 0
        if 'color' not in sample:
            dia.add_sample(sample['std'], sample['corrcoef'],
                       marker=sample['marker'], ms=8, ls='',
                       zorder=sample['zorder'],
                       label=sample['name'])
        else:
            dia.add_sample(sample['std'], sample['corrcoef'],
                       marker=sample['marker'], ms=8, ls='',
                       mfc=sample['color'], mec=sample['color'],
                       zorder=sample['zorder'],
                       label=sample['name'])        
        handles_dictionary[sample['name']] = sample['color']
    handles = [mpatches.Patch(color=color, label=name) for name, color in handles_dictionary.items()]
    handles = [mpatches.Patch(color='b')] + handles
    labels = [obs_label] + list(handles_dictionary.keys())   
  
    fig.legend(handles, labels, numpoints=1, loc='upper right')

    for sample in unlabelled_data:         
        if sample['corrcoef'] < 0:
            sample['corrcoef'] = 0
        if 'color' not in sample:
            dia.add_sample(sample['std'], sample['corrcoef'],
                       marker=sample['marker'], ms=8, ls='',
                       zorder=sample['zorder']-1,
                       label=sample['name'])
        else:         
            dia.add_sample(sample['std'], sample['corrcoef'],
                       marker=sample['marker'], ms=8, ls='',
                       mfc=sample['color'], mec=sample['color'],
                       zorder=sample['zorder']-1,
                       label=sample['name'])

    dia.add_grid(True, axis='x', linestyle='--', alpha=0.6,
                 color='0.5', zorder=0)
                  
    contours = dia.add_contours(colors='0.5')
    plt.clabel(contours, inline=1, fontsize=10)

    if 'title' in ax_args:
        plt.title(ax_args['title'])    
    if label is not None:
         fig.text(0.1, 0, label, fontsize=7)       
          
           
def histogram(data, values, ax=None, ax_args=None, plot={}):
    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    else:
        fig = plt.gcf()
    
    n, bins, patches = plt.hist(data, 10, facecolor='grey', alpha=0.75, edgecolor='black')
    ymax = int(ceil(1.2 * max(n)))
    ax.set_ylim(0, ymax)
    
    labels = []
    for sample in values:
        if not sample['name'] in labels:
            # add new label
            label = sample['name']
            labels.append(label)
        else: 
            # label won't be recorded in legend
            label = "" 
        plt.axvline(sample['data'], label=label, linewidth=4,
                    color=sample['color'])
    plt.setp(ax, **ax_args)
    if 'title' in ax_args:
        plt.title(ax_args['title'])
    
    ax.legend(loc='best')
    
def scatter(data, data2, ax=None, ax_args=None, plot={}):
    if not ax:
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    else:
        fig = plt.gcf()
    x = data.flatten()
    y = data2.flatten()
    ax.scatter(x, y, marker='.', c='0.5', s=0.3)
    if ax_args:
        plt.setp(ax, **ax_args)  

if __name__ == "__main__":
    pass
