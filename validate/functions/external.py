import os
import cdo
cdo = cdo.Cdo()

def split(name):
    """ Returns the name of a file without the directory path
    """
    path, filename = os.path.split(name)
    return filename

def sample(ifile, **kwargs):    
    print('called external function')
    return ifile
    
def field_integral(ifile, **kwargs):
    out = 'netcdf/field-integral_' + split(ifile)
    fout = 'netcdf/gridarea_' + split(ifile)
    mout = 'netcdf/mul_' + split(ifile)
    ymean = 'netcdf/yrmean' + split(out)
    cdo.gridarea(input=ifile, output=fout)
    cdo.mul(input=ifile + ' ' + fout, output=mout)
    cdo.fldsum(input=mout, output=out)
    cdo.yearmean(input=out, output=ymean)
    return ymean


def anomaly(ifile, **kwargs):
    ifile_split = split(ifile)

    out = 'netcdf/anomaly_' + ifile_split
    ymean = 'netcdf/yrmean_' + ifile_split
    gmean = 'netcdf/glmean_' + ifile_split
    tmean = 'netcdf/timmean_' + ifile_split
    #cdo.gridarea(input=ifile, output=fout)
    cdo.yearmean(input=ifile, output=ymean)
    cdo.fldmean(input=ymean, output=gmean)
    cdo.timmean(input=gmean, output=tmean)
    cdo.sub(input=gmean + ' ' + tmean, output=out) #not sure about this one
    return out
