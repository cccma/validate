#!/bin/python
"""

Produces plots using validate package.

"""
import argparse
import getpass
import os
import sys
import validate.control as control

def main():
    opts = args() 
    if opts['batch']:
        check_user_environment()
        build_job_script(opts)
        sub_job_script()
    else:
        #execute interactively
        control.execute(opts)

def args():
    description = 'Produces plots using validate package'
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-l', '--link_plots_to_viewer',
                        action='store_true',
                        help="Automatically soft link the produced plots to your ~/public_html directory so you\
                                can view them on https://goc-dx.science.gc.ca/~SciID/, where SciID should be replaced\
                                by your science ID")
    parser.add_argument('-d', '--debugging', action='store_true', 
                        help="debugging option")
    parser.add_argument('-b', '--batch', action='store_true',
                        help="execute in batch mode. This generates a bash script to run validate and submits it to the batch \
                                queueing system. A zip file of your plots will be placed in the directory specified as \
                                output_root in your conf file.")

                    
    args = parser.parse_args()
    return vars(args)

def check_user_environment():
    exit_flag = False
    env_dict = os.environ

    #check if jobsub is available to user, tail end of string suppresses
    #the standard output of os.system call
    if not os.WEXITSTATUS(os.system("which jobsub >/dev/null 2>&1")) == 0:
        print(('Your environment does not seem to have access to \'jobsub\'. Please activate the '\
                'CCCma environment to access it, or activate the ORDENV '\
                'environment and load the \'jobctl\' utilities. See the following links for information:\n'\
               '- for activating ORDENV: '\
                'https://portal.science.gc.ca/confluence/display/SCIDOCS/Setting+Up+the+Environment#SettingUptheEnvironment-LatestConfiguration\n'\
               '- for getting the \'jobctl\' utilities: '\
                'https://portal.science.gc.ca/confluence/display/SCIDOCS/Using+jobctl\n'))
        exit_flag = True

    if 'CCRNTMP' not in env_dict:
        print(("The $CCRNTMP environemnt variable isn't set, please set it to"
               "/space/hall3/sitestore/eccc/crd/ccrn/../ccrn_tmp"
               "or /space/hall4/sitestore/eccc/crd/ccrn/../ccrn_tmp"
               "depending on what hall you want to run on\n"))
        exit_flag = True
    if 'TRUE_HOST' not in env_dict:
        print("The $TRUE_HOST environemt variable is not set, please set it to ppp3 or ppp4 depending on what hall you want to run on")
        exit_flag = True

    if exit_flag:
        sys.exit()


def build_job_script(opts):
    ID = getpass.getuser()
    OUTPUT_DIR = os.path.abspath(os.getcwd())
    RES_IMAGE = 'eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest'
    CCRNTMP = os.environ['CCRNTMP']
    CONDA_ENV = os.environ['CONDA_DEFAULT_ENV'] # get name of current running environment, to use in jobscript

    link_plots_to_viewer = ''
    ignore = ''
    debug = ''
    
    #if argument is present in original call, inject it into the call in jobscript
    if opts['link_plots_to_viewer']:
        link_plots_to_viewer = '-l'
    if opts['debugging']:
        debug = '-d'
    #generate bash script, strings enclosed in {} are treated as variables
    with open ('validate_jobscript', 'w') as jobScript:
        jobScript.write((
        f'#!/bin/bash\n'
        f'#\n'
        f'#   Validate job script\n'
        f'#\n'
        f'#PBS -q development\n'
        f'#PBS -N {ID}_validate_plots\n'
        f'#PBS -j oe\n'
        f'#PBS -o {OUTPUT_DIR}/validatelog\n'
        f'#PBS -l walltime=03:00:00\n'
        f'#PBS -l select=1:ncpus=1:mem=15gb:res_image={RES_IMAGE}\n'
        f'\n'
        f'# Specify working directory\n'
        f'wrk_dir="{CCRNTMP}/tmp_validate_$$_`date +%Y%m%d%H%M%S%3N`"\n'
        f'\n'
        f'# get conda\n'
        f'export PATH=/home/ords/crd/ccrn/scrd102/ccc_conda/4.6.14/miniconda2/bin:$PATH\n'
        f'\n'
        f'# activate python env\n'
        f'source activate {CONDA_ENV}\n'
        f'\n'
        f'# navigate to temporary working directory\n'
        f'mkdir $wrk_dir\n'
        f'cd $wrk_dir\n'
        f'\n'
        f'# get config file and log it\n'
        f'cp {OUTPUT_DIR}/conf.yaml .\n'
        f'if [ $? -eq 0 ]; then\n'
        f'    echo "============ Configuration File ================"\n'
        f'    cat conf.yaml\n'
        f'    echo "================================================"\n'
        f'else\n'
        f'    echo "conf.yaml could not be found"\n'
        f'    cd {OUTPUT_DIR}\n'
        f'    rm -rf $wrk_dir\n'
        f'fi\n'
        f'\n'
        f'# plot\n'
        f'validate-execute {link_plots_to_viewer} {debug}\n'
        f'if [ $? -eq 0 ]; then\n'
        f'    echo "Job successfull"\n'
        f'    cd {OUTPUT_DIR}\n'
        f'    rm -rf $wrk_dir\n'
        f'else\n'
        f'    echo "Job failed"\n'
        f'    echo "Temporary directory not deleted and located:"\n'
        f'    echo -e "\\t ${{wrk_dir}}"\n'
        f'fi\n'
        ))

def sub_job_script():
    print(f"running in batch mode, output files will be deposited in this folder")
    os.system("jobsub -c $TRUE_HOST validate_jobscript")

if __name__=="__main__":
    main()
